
#+STARTUP: showall

#+TITLE: Eclats de vers : Matemat 03 : Nombres - 2
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

#+INCLUDE: "../include/commandes-tex.org"

* Entiers

#+TOC: headlines 1 local

\label{chap:entiers}


** Dépendances

  - Chapitre \ref{chap:naturels} : Les nombres naturels


** Principe général de l'extension

Nous allons à présent étendre progressivement les opérations vues sur les naturels. Le principe est de partir d'un ensemble $X$ et de construire un ensemble dérivé $Y$ (souvent $Y$ sera $X^2$). Ensuite, nous définissons l'opération étendue sur $Y$ de telle sorte qu'elle vérifie les nouvelles propriétés demandées en plus de celles déjà acquises sur $X$.


** Soustraction de naturels

On aimerait bien étendre la soustraction à deux naturels $i,j \in \setN$ quelconques. Malheureusement, nous avons vu que si $i \le j$, cette opération n'est pas définie. Pour contourner ce problème nous introduisons la notation différentielle :

$$i - j = (i,j)$$

où $(i,j) \in \setN^2$ est appelé nombre entier. Il ne nous reste plus ensuite qu'à définir convenablement les autres opérations pour conserver les propriétés intéressantes qu'elles possèdent sur $\setN$.


*** Notations

Soit $i \in \setN$. On note aussi :

#+BEGIN_CENTER
\(
i = i - 0 = (i,0) \\
-i = 0 - i = (0,i)
\)
#+END_CENTER

En particulier :

#+BEGIN_CENTER
\(
1 = (1,0) \\
0 = (0,0) \\
-1 = (0,1)
\)
#+END_CENTER


** Addition

Soit $i,j,k,l \in \setN$. Pour conserver l'associativité et la commutativité, on doit avoir :

$$(i - j) + (k - l) = (i + j) - k - l = (i + j) - (k + l)$$

Ceci nous incite à définir l'addition des entiers par :

$$(i,j) + (k,l) = (i + j, k + l)$$


*** Neutre additif

Soit $i,j \in \setN$. On a :

$$(i - j) + (0 - 0) = (i + 0) - (j + 0) = i - j$$

Pour tout $n \in \setN$, la soustraction native des naturels nous dit que :

$$n - n = 0$$

Afin de rester consistant, on impose que :

$$i - j = (i - j) + 0 = (i - j) + (n - n) = (i + n) - (j + n)$$

ou, en terme de couples :

$$(i,j) = (i + n, j + n)$$

Le neutre pour l'addition s'écrit donc :

$$0 = 0 - 0 = n - n = (0,0) = (n,n)$$


*** Équivalence

On voit apparaître les familles :

$$D(i,j) = \{ (i + n, j + n) : n \in \setN \}$$

où chaque élément de $D(i,j)$ est équivalent à un autre.
Nous noterons donc également :

$$i - j = D(i,j)$$


** Définition

On définit l'ensemble des nombres entiers par :

$$\setZ = \{ i - j : i,j \in \setN \}$$

où $i - j$ représente l'ensemble d'équivalence $D(i,j)$.
En introduisant les symboles usuels, on a donc :

$$\setZ = \{ ...,-5,-4,-3,-2,-1,0,1,2,3,4,5,... \}$$


*** Forme canonique

Soit $i,j \in \setN$.


  - Si $i \ge j$, le naturel $i - j$ existe et nous pouvons toujours
ramener $(i,j)$ par équivalence à :

$$(i - j, 0) = (i - j, 0) + (j,j) = (i - j + j, j) = (i,j)$$

On dit alors que $(i,j) = i - j$ est un entier positif.

  - Si $i \le j$, le naturel $j - i$ existe et nous pouvons toujours
ramener $(i,j)$ par équivalence à :

$$(0, j - i) = (0, j - i) + (i,i) = (i, j - i + i) = (i,j)$$

On dit alors que $(i,j) = i - j$ est un entier négatif.



*** Signe

On définit l'ensemble des entiers positifs par :

$$\setZ^+ = \{ i = D(i,0) : i \in \setN \}$$

ainsi que l'ensemble des entiers négatifs :

$$\setZ^- = \{ -i = D(0,i) : i \in \setN \}$$

L'ensemble des entiers est bien entendu l'union des deux : $\setZ = \setZ^+ \cup \setZ^-$.


*** Inclusion

Nous pouvons associer à tout naturel $i$ un entier équivalent $i - 0 = (i,0)$. Pour cette raison, nous dirons que tout naturel est également un entier, et nous noterons : $\setN \subseteq \setZ$.


*** Entiers positifs et naturels

Il existe une bijection $f : \setN \mapsto \setZ^+$ définie par :

$$f(n) = (n,0) = n - 0 = n$$

pour tout $n \in \setN$. On assimile donc les deux ensembles en écrivant
$\setN = \setZ^+$.


** Opposé

Soit $i,j \in \setN$. On déduit de la définition de l'addition que :

$$(i - j) + (j - i) = (i + j) - (j + i) = (i + j) - (i + j) = 0 - 0 = 0$$

ou, en terme de couples :

$$(i,j) + (j,i) = (i + j, i + j) = (0,0) = 0$$

On dit que $j - i = (j,i)$ est l'opposé de $i - j = (i,j)$,
et inversément. On le note :

$$- (i - j) = j - i$$

ou :

$$- (i,j) = (j,i)$$


*** De l'opposé

On a clairement :

$$- ( - (i - j)) = - (j - i) = i - j$$

ou :

$$- ( - (i,j)) = - (j,i) = (i,j)$$

L'opposé de l'opposé est l'entier lui-même.


*** D'une somme

Soit $u,v \in \setZ$ et $i,j,k,l \in \setN$ tels que :

$$u = i - j$$
$$v = k - l$$

On a :

$$(u + v) + ((-u) + (-v)) = ((i - j) + (k - l)) + ((j - i) + (l - k))$$

ce qui nous donne :

$$(u + v) + ((-u) + (-v)) = (i + k + j + l) - (j + l + i + k) = 0$$

On en conclut que la somme des opposés est égale à l'opposé de la somme :

$$(-u) + (-v) = - ( u + v)$$


*** Notation

On note aussi :

$$- u + v = (-u) + v$$

pour tout $u,v \in \setZ$.


*** Moins un

Un cas particulier important :

$$-(1 - 0) = 0 - 1 = -1$$

et :

$$-(-1) = 1$$


** Soustraction d'entiers

Soit $i,j,k,l \in \setN$. Comme :

\begin{align}
i + (-j) = (i,0) + (0,j) = (i,j) = i - j
\end{align}

On étend la soustraction à l'ensemble des entiers par :

\begin{align}
(i,j) - (k,l) = (i,j) + (-(k,l)) = (i,j) + (l,k)
\end{align}

ce qu'on peut réécrire par :

$$u - v = u + (-v)$$

pour tout $u,v \in \setZ$.


*** Propriétés

On a clairement :

$$(-i) - j = (-i) + (-j) = - ( i + j )$$

$$(-i) - (-j) = (-i) + j = j + (-i) = j - i$$


*** Notation

On note aussi :

$$- u - v = (-u) - v$$

pour tout $u,v \in \setZ$.


** Ordre

Soit $u,v \in \setZ$ et $i,j,k,l \in \setN$ tels que :

$$u = i - j$$
$$v = k - l$$

Si nous voulons conserver la propriété de conservation
de l'ordre sous l'addition, l'inégalité :

$$u = i - j \le k - l = v$$

doit être équivalente à :

$$(i - j) + j + l \le (k - l) + j + l$$

qui nous donne :

$$i + l \le k + j$$

Nous définissons l'ordre sur les entiers en affirmant que :

$$i - j \le k - l$$

si et seulement si :

$$i + l \le j + k$$


*** Plus grand ou égal

Soit $x,y \in \setZ$. On note aussi :

$$y \ge x$$

pour signifier que $x \le y$.


*** Ordre strict

Soit $x,y \in \setZ$. On note :

$$x \strictinferieur y$$

$$y \strictsuperieur x$$

lorsque $x \le y$ et $x \ne y$.


*** Conservation

Soit $u,v,w,z \in \setZ$ et $i,j,k,l,m,n,r,s \in \setN$ tels que :

$$u = i - j$$
$$v = k - l$$
$$w = m - n$$
$$z = r - s$$

On a :

$$u + w = (i + m) - (j + n)$$
$$v + z = (k + r) - (l + s)$$

Supposons que :

$$u \le v$$
$$w \le z$$

On a :

$$i - j \le k - l$$
$$m - n \le r - s$$

ou :

$$i + l \le j + k$$
$$m + s \le n + r$$

L'ordre des naturels étant conservé sous l'addition, on a :

$$i + l + m + s \le j + k + n + r$$

et :

$$(i + m) - (j + n) \le (k + r) - (l + s)$$

c'est-à-dire :

$$u + w \le v + z$$

L'ordre des entiers est conservé par l'addition.


*** Opposé

Soit $u,v \in \setZ$ tels que :

$$u \le v$$

Comme :

$$-(u + v) = -(u + v)$$

on a :

$$-(u + v) \le -(u + v)$$

et :

$$u + (-(u + v)) \le v + (-(u + v))$$

En développant, on a :

$$-v = u - u - v \le v - u - v = -u$$

c'est-à-dire :

$$-u \ge -v$$

L'ordre sur les opposés est l'inverse de l'ordre original.


**** Signe

Soit $z \in \setZ$. Si $z \ge 0$, on a :

$$-z \le 0$$

Inversément, si $z \le 0$, on a :

$$-z \ge 0$$


*** Positifs et négatifs

Soit $u \in \setZ^+$ et $i \in \setN$ tel que :

$$u = i - 0$$

On a :

$$i + 0 \ge 0 + 0$$

et :

$$i - 0 \ge 0 - 0 = 0$$

On en déduit que $u \ge 0$. Réciproquement, soit $u \in \setZ$ et
$i,j \in \setN$ tels que :

$$u = i - j$$

Si $u \ge 0$, on a :

$$i - j \ge 0$$

et :

$$i \ge j$$

On peut donc mettre $u$ sous la forme canonique :

$$u = (i - j,0) \in \setZ^+$$

On a donc :

$$\setZ^+ = \{ u \in \setZ : u \ge 0 \}$$

On montre aussi que :

$$\setZ^- = \{ u \in \setZ : u \le 0 \}$$

Par analogie avec les naturels, on dit que les entiers positifs
sont les successeurs de $0$ et les entiers négatifs les
prédécesseurs de $0$.


** Signe

La fonction signe est définie par :

$$\signe(z) =
\begin{cases}
1 & \text{si } \ z \ge 0 \\
-1 & \text{si } \ z \strictinferieur 0
\end{cases}$$

pour tout $z \in \setZ$.


** Valeur absolue

Soit $z \in \setZ$. On définit la valeur absolue de $z$ par :

$$\left| z \right | = \max \{ z , -z \}$$

On a donc :

$$\left| z \right| =
\begin{cases}
z & \text{si } \ z \ge 0 \\
-z & \text{si } \ z \strictinferieur 0
\end{cases}$$

Comme le nombre positif l'emporte toujours sur le négatif dans le maximum, on a :

$$\left| z \right| \ge 0$$

On vérifie que :

$$\left| x + y \right| \le \left| x \right| + \left| y \right|$$

pour tout $x,y \in \setZ$.


** Multiplication

Soit $u \in \setZ$. Le naturel $0$ est absorbant pour la multiplication.
On souhaite conserver cette propriété pour l'entier $0 = 0 - 0$ :

$$u \cdot 0 = 0 \cdot u = 0$$

$$u \cdot 1 = 1 \cdot u = u$$

Soit $u,v \in \setZ$. Si nous voulons conserver la distributivité, il
faut que :

$$u \cdot v + (-u) \cdot v = (u + (-u)) \cdot v = 0 \cdot v = 0$$

On en conclut que :

$$(-u) \cdot v = - (u \cdot v)$$

On a aussi :

$$u \cdot v + u \cdot (-v) = u \cdot (v + (-v)) = u \cdot 0 = 0$$

et :

$$u \cdot (-v) = -(u \cdot v)$$

Enfin :

$$(-u) \cdot (-v) = - (u \cdot (-v)) = - ( - (u \cdot v)) = u \cdot v$$


*** Définition

Soit $u,v \in \setZ$ et $i,j,k,l \in \setN$ tels que :

$$u = i - j$$
$$v = k - l$$

On a :

$$u \cdot v = (i - j) \cdot (k - l) = (i + (-j)) \cdot (k + (-l))$$

La distributivité nous donne :

$$u \cdot v = i \cdot (k + (-l)) + (-j) \cdot (k + (-l))$$

En l'utilisant une nouvelle fois, on arrive à :

$$u \cdot v = i \cdot k + i \cdot (-l) + (-j) \cdot k + (-j) \cdot (-l)$$

ou :

$$u \cdot v = i \cdot k - i \cdot l - j \cdot k + j \cdot l = (i \cdot k + j \cdot l) - (i \cdot l + j \cdot k)$$

On définit donc la multiplication d'entiers par :

$$(i,j) \cdot (k,l) = (i \cdot k + j \cdot l, i \cdot l + j \cdot k)$$


*** Entiers positifs

Soit $u,v \in \setZ$ tels que $u,v \ge 0$. On peut trouver des naturels
$i,j$ tels que :

$$u = i - 0$$
$$v = j - 0$$

On a :

$$u \cdot v = (i - 0) \cdot (j - 0) = (i \cdot j + 0 \cdot 0) - (i \cdot 0 + 0 \cdot j) = i \cdot j - 0 = i \cdot j$$

La multiplication d'entiers positifs correspond à celle des naturels.


*** Lien avec l'addition

Soit $u,v \in \setZ$ avec $v \ge 0$ et $i,j,k \in \setN$ tels que :

$$u = i - j$$
$$v = k - 0$$

On a :

$$u \cdot v = (i - j) \cdot (k - 0) = (i \cdot k + j \cdot 0) - (j \cdot k + i \cdot 0) = i \cdot k - j \cdot k$$


En additionnant $k$ fois le même terme $u$, on obtient :

$$u + ... + u = i \cdot k - j \cdot k = u \cdot v$$


*** Commutativité

La commutativité de la multiplication sur les entiers découle de celle sur les naturels :

\begin{align}
(k - l) \cdot (i - j) &= (k \cdot i + l \cdot j) - (k \cdot j + l \cdot i) \\
&= (i \cdot k + j \cdot l) - (i \cdot l + j \cdot k) \\
&= (i - j) \cdot (k - l)
\end{align}


*** Associativité

Soit $u,v,w \in \setZ$. Si $u, v, w \ge 0$, on peut les associer aux naturels
$i,j,k \in \setN$ par :

$$u = i - 0$$
$$v = j - 0$$
$$w = k - 0$$

On a alors :

$$u \cdot v = i \cdot j$$

et :

$$v \cdot w = j \cdot k$$

On a donc :

$$(u \cdot v) \cdot w = (i \cdot j) \cdot (k - 0) = i \cdot j \cdot k - 0 = i \cdot j \cdot k$$

et :

$$u \cdot (v \cdot w) = (i - 0) \cdot (j \cdot k) = i \cdot j \cdot k - 0 = i \cdot j \cdot k$$

On en conclut que :

$$(u \cdot v) \cdot w = u \cdot (v \cdot w)$$

Si un ou plusieurs entiers sont négatifs, soit $-u,-v,-w$, on a :

$$((-u) \cdot v) \cdot w = (- (u \cdot v)) \cdot w = - ((u \cdot v) \cdot w)$$

et :

$$((-u) \cdot v) \cdot w = - (u  \cdot (v \cdot w)) = (-u) \cdot (v \cdot w)$$

ou :

$$(u \cdot v) \cdot (-w) = - ((u \cdot v) \cdot w)$$

et :

$$(u \cdot v) \cdot w = - (u  \cdot (v \cdot w)) = u \cdot (-(v \cdot w)) = u \cdot (v \cdot (-w))$$

Les autres cas sont semblables, on a donc :

$$u \cdot v \cdot w = (u \cdot v) \cdot w = u \cdot (v \cdot w)$$

pour tout $u,v,w \in \setZ$.


*** Neutre

On a :

$$(i - j) \cdot 1 = (i - j) \cdot (1 - 0) = (i \cdot 1 + j \cdot 0) - (i \cdot 0 + j \cdot 1) = i - j$$

et :

$$1 \cdot (i - j) = (i - j) \cdot 1 = i - j$$

L'entier $1 = 1 - 0$ est le neutre pour la multiplication.


*** Notation

On note aussi :

$$- u \cdot v = - (u \cdot v)$$

pour tout $u,v \in \setZ$.


*** Moins un

On a :

$$(-1) \cdot (i,j) = - (1 \cdot (i,j)) = - (i,j)$$

Un cas particulier important :

$$(-1) \cdot (-1) = -(-1) = 1$$


** Ordre et multiplication


*** Conservation simple

Soit $x,y,z \in \setZ$ avec $z \ge 0$. Supposons que :

$$x \le y$$

On a alors :

$$x \cdot z = x + ... + x \le y + ... + y = y \cdot z$$

L'ordre est conservé lorsqu'on multiplie les deux membres de l'inégalité par un entier positif. Par contre :

$$x \cdot (-z) = - x \cdot z \ge - y \cdot z = y \cdot (-z)$$

L'ordre est inversé lorsqu'on multiplie les deux membres de l'inégalité par un entier négatif.


*** Conservation double

Soit $x,y,u,v \in \setZ$ vérifiant :

$$x \le y$$
$$u \le v$$

Si :

$$x,y,u,v \ge 0$$

on a :

$$x \cdot u \le x \cdot v \le y \cdot v$$


** Notation décimale

Soit le tuple :

$$(i_0,i_1,i_2,...i_{n - 1},i_n) \in \{ 0,1,2,3,4,5,6,7,8,9 \}^{n + 1}$$

La notation décimale associée est définie par :

$$i_n i_{n - 1} ... i_2 i_1 i_0 = i_0 + i_1 \cdot 10 + i_2 \cdot 10^2 + ... + i_{n - 1} \cdot 10^{n - 1} + i_n \cdot 10^n$$

pour les entiers positifs et :

$$-i_n i_{n - 1} ... i_2 i_1 i_0 = -\left( i_0 + i_1 \cdot 10 + i_2
\cdot 10^2 + ... + i_{n - 1} \cdot 10^{n - 1} + i_n \cdot 10^n
\right)$$

pour les entiers négatifs. Exemple :

$$-7512 = -\left( 2 + 1 \cdot 10 + 5 \cdot 10^2 + 7 \cdot 10^3 \right)$$


** Division entière et modulo

Soit $m,n \in \setN$, avec $n \ne 0$. Soit :

\begin{align}
k &= m \diventiere n \\
r &= m \modulo n
\end{align}

On veut étendre la division entière à $\setZ$ en conservant la décomposition :

$$m = k \cdot n + r$$

On note que :

$$m = (-k) \cdot (-n) + r$$

On en déduit l'extension :

$$m \diventiere (-n) = -k = - (m \diventiere n)$$

et :

$$m \modulo (-n) = r = m \modulo n$$

On note que :

$$-m = k \cdot (-n) - r$$

On en déduit l'extension :

$$(-m) \diventiere (-n) = k = m \diventiere n$$

et :

$$(-m) \modulo (-n) = -r = - (m \modulo n)$$

On note que :

$$-m = (-k) \cdot n - r$$

On en déduit l'extension :

$$(-m) \diventiere n = -k = -(m \diventiere n)$$

et :

$$(-m) \modulo n = -r = - (m \modulo n)$$


** Puissance

Soit $z \in \setZ$ et $n \in \setN$. On définit les puissances par :

\begin{align}
z^0 &= 1 \\
z^n &= z \cdot z^{n - 1}
\end{align}

On a donc :

$$z^n = z \cdot ... \cdot z$$

$z^n$ est égal au produit de $n$ facteurs $z$.


** Anneau

$(\setZ,+,\cdot)$ est un anneau.


** Propriétés

La majeure partie des résultats démontrés sur $\setN$ reste valable sur $\setZ$. On le vérifie aisément en utilisant les définitions étendues. Exceptions :

  - La positivité des naturels (voir section \ref{sec:positivite_des_naturels}), qui est remplacée par la positivité de la valeur absolue
