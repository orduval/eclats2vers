
#+STARTUP: showall

#+TITLE: Eclats de vers : Poésie : Incendie
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index littéraire]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Crochet

#+BEGIN_CENTER
#+BEGIN_VERSE
    Un nid de notes
    Tombe de l’arbre
    Il dégringole
    Et se lézarde

    Sans prendre garde
    A cette chute
    La belle harpe
    Etreint son luth

    La jolie flûte
    En devient rouge
    Elle est jalouse
    Dis la clef d’ut

    C’est la salsa
    Des andalouses
    Entre le blues
    Et la rumba

    C’est l’opéra
    D’une tzigane
    Habanera
    De La Havane

    Une coquine
    Qui se pavane
    La mandoline
    Qui a pris flamme

    Une pluie fine
    Sur de la braise
    Que rien n’apaise
    Et qui s’obstine

    C’est un dièse
    Désaltéré
    Et parfumé
    D’un rien de fraise

    Douceur fruitée
    Sur fond de poivre
    Hors de portée
    Dans les nuages

    Crue syncopée
    Vague solfège
    Tri-crochetée
    Par un arpège

    Dans la nuance
    D’un bémol beige
    Et la fournaise
    Des décadences

    Viennent les rondes
    La phase change
    Rythme et cadence
    Mènent le monde

    Au fil des tons
    Qui se confondent
    De la passion
    Aux tendres blondes

    Telle est la gamme
    Des pulsations
    Canon pour dame
    Salve pulsions

    Tel est l’accord
    Qui nous désarme
    En cerfs qui brâment
    Au son du cor

    Le clavier mord
    Dans les octaves
    Teste les graves
    Prend son essor

    Hache l'échelle
    De ses saccades
    Se glisse au ciel
    Coule la cave

    Puis il module
    Sa mélodie
    La mode cule
    Clouée saisie

    Tétanisée
    Par cette averse
    L’harmonie berce
    Sa léthargie

    Coupe anisée
    Qui la tamise
    Puis se renverse
    Perverse exquise

    Quelques mesures
    De frénésie
    De démesure
    Et de folie

    Une fanfare
    De fantaisie
    Flots en furie
    Dans la nuit noire

    Orgue et cithare
    Ensemble jasent
    Est-il si tard ?
    L’heure du jazz

    Postons ces lettres
    Choisis un timbre
    Ah faut-il l'être
    Etre un peu dingue

    Changeons de thème
    Plus lent moins sage
    Joue contre joue
    D’autres orages

    Faut-il qu’on l’aime
    Pour qu’on la joue
    Qu’on la déjoue
    Et qu’on s’y baigne

    Tâter l’aiguë
    Frôler la grave
    A moitié nue
    Panthère en cage

    Mais tout s’achève
    L’aura les rêves
    Aux fleurs d’aurore
    Le corps s’endort

    Dernières notes
    Evanescence
    C’est le point d’orgue
    Puis le silence
#+END_VERSE
#+END_CENTER

* Sonnetine

#+BEGIN_CENTER
#+BEGIN_VERSE
    Sur la grand-place aux lis où fleurit l’anathème,
    Un lit fleurdelisé réchauffe un peu de cendre.
    Près de l'âtre, un tison respire et sa cassandre,
    Sa marâtre maîtresse effeuille un chrysanthème.

    La boiserie laquée, le sommier palissandre
    Ont vu bien des laquais se payer de satin.
    Seul, un vieux sommelier n’a voulu condescendre
    A boire le vin blanc des collines d’airain.

    C’est un clairet très frais qui coule sans pépin,
    Un fleuve d’or qui prend source sous la cambrure
    Mais la vase, l’ignare, exècre les dorures,
    Puisqu’au moindre vent d’ange, elle noie les raisins.

    C’est le sort réservé aux terrestres liqueurs
    Que de n'être qu’un temps l’embrun gris et vainqueur,
    De mêler l’ambre gris aux rougeurs des framboises.

    Quant à tous ces gardiens de vertu et leurs ordres,
    Ils ne font qu'épicer une pudeur grivoise :
    Dans tout vertugadin, les désirs sont désordres.

    Eau, céans défie-toi de ces filles matoises,
    Aile et gant, ce sont là leurs crocs et leurs babines :
    Elles n’ont pas tôt dit qu’elles vous embobinent.
#+END_VERSE
#+END_CENTER

* Les 7 pêchers capiteux

#+BEGIN_CENTER
#+BEGIN_VERSE
    C’est l’orgueil de savoir que je tiens dans les mains
    Ces flammes qui te font rougir quand la nuit tombe
    Et qu’au creux de ton lit tu explores tes reins
    Te tordant de plaisir quand mon chant te féconde
    C’est l’orgueil de savoir que je tiens dans les mains
    Ces flammes qui te font rougir quand la nuit tombe

    C’est l’avarice aussi je garde les caresses
    Dans un coffre blindé à l’abri des jaloux
    Je frappe la monnaie de ta belle tendresse
    Pour t'étendre en douceur sur la planche à bisous
    C’est l’avarice aussi je garde les caresses
    Dans un coffre blindé à l’abri des jaloux

    Puis c’est la gourmandise à te dévorer crue
    Marinée en brochette ou sautée à la poële
    Qu’importe la cuisson lorsque je te vois nue
    Rosée ou cuite à point j’ai comme une fringale
    Puis c’est la gourmandise à te dévorer crue
    Marinée en brochette ou sautée à la poële

    La colère parfois juste pour l’arc-en-ciel
    Arroser de sanglots la torpeur de l'été
    Qu’importe le prétexte à épancher le fiel
    C’est si bon de guérir en se laissant soigner
    La colère parfois juste pour l’arc-en-ciel
    Arroser de sanglots la torpeur de l'été

    L’envie de toi qui rôde et s’infiltre partout
    Comme un lourd pare-faim dans un soir de folie
    Quand l’orage à venir électrise les doux
    Nuages surchargés n’attendant que la pluie
    L’envie de toi qui rôde et s’infiltre partout
    Comme un lourd pare-faim dans un soir de folie

    La paresse souvent quand il faut te quitter
    Dans le blême matin des fins d’après-midi
    Et loin du nid douillet parcourir les sentiers
    Glacials de ces zombies qui méprisent la vie
    La paresse souvent quand il faut te quitter
    Dans le blême matin des fins d’après-midi

    La luxure toujours de la cave au grenier
    S’enrouler dans la paille ou dans les herbes folles
    Se laisser envahir de chaudes voluptés
    Ou saccager un lit quand le désir décolle
    La luxure toujours de la cave au grenier
    S’enrouler dans la paille ou dans les herbes folles
#+END_VERSE
#+END_CENTER

* Plume d'amande

#+BEGIN_CENTER
#+BEGIN_VERSE
    Les nuées pourpres du désir
    Viennent rosir le parchemin
    Son coeur se gonfle d’un soupir
    Le stylo languit dans sa main
    Quelle strophe vive et sanguine
    Ecrit-elle à l’encre carmin ?
    J’entends glisser sur le vélin
    Son joli rire de coquine

    Les nuées pourpres du désir
    Se sont lovées dans ses cheveux
    Sa muse d’un baiser fougueux
    Baillonne son charmant sourire
    C’est un frôlement langoureux
    Qui l’envoûte et qui la taquine
    C’est le murmure des aveux
    Son joli rire de coquine

    Les nuées pourpres du désir
    L’encerclent d’un ruban de flammes
    L’air surchauffé par le plaisir
    Soulève sa robe de femme
    En scandant la danse de l'âme
    Aux cadences qui la calcinent
    Jusqu’au sommet où il se pâme
    Son joli rire de coquine

    Les nuées pourpres du désir
    Tordent les cordes de la lyre
    Lorsque dans l’alcôve en délire
    L’ombre de la chair se devine
    On l’attend on en redemande
    La fleur ardente de l’amande
    Et sa jolie plume gourmande
    Son joli rire de coquine
#+END_VERSE
#+END_CENTER

* Double tranchant

#+BEGIN_CENTER
#+BEGIN_VERSE
    Parfois quand le silence envahit l’océan
    C’est que le vent qui sait le sel vif des morsures
    A peur de déchirer la fragile voilure
    C’est qu’il voudrait hurler et se tait en tremblant

    C’est qu’il voudrait pouvoir aimer sans pour autant
    Blesser ni se blesser croire comme autrefois
    Qu’il peut refermer ses deux mains sur ses dix doigts
    La serrer sans briser cet espoir qu’on lui tend

    Sans transformer la joie en songe évanescent
    Mais aimer c’est saisir une épée double lame
    La poser sur le mur qui sépare nos âmes
    Et nous dire tous deux viens mon coeur je t’attends

    Mais aimer c’est tisser une autre déchirure
    Sur la lune satin sur la soie de l’azur
    Oubliant pour un temps ce que le temps élime

    C’est avoir le courage indomptable et la foi
    De ceux qui veulent croire aux sentiments sublimes
    Et qui disent c’est l’aube au soleil qui rougeoie

    C’est vouloir s’envoler vers les plus hautes cimes
    Planer sans carburant quand le vent nous réclame
    Au-dessus des volcans que notre lave enflamme
#+END_VERSE
#+END_CENTER

* Caducée

#+BEGIN_CENTER
#+BEGIN_VERSE
    Craindre de raviver la vigueur des morsures,
    Effleurer prudemment les jalouses forêts
    Hantées par les esprits des fantasmes secrets,
    C’est pour les tendres dents de lait de la luxure.

    Un cupidon cupide empoisonne nos coeurs :
    La suave saveur du piment clandestin,
    L’arme à double tranchant des amours serpentins
    Sont de nos sangs trop froids les ultimes chaleurs.

    Des ébats de cobras, effrénés et languides,
    Quelques frémissement d’exploration tactile
    Suivis par la fureur des frottements reptiles,
    Voilà ce qui convient à nos passions bifides !

    Viens ma soeur, dégustons les plaisirs interdits,
    Que les anneaux sournois des fleurs incestueuses
    S’enroulent à couvert dans l’ombre sinueuse
    Pour combler dans l’orgie leur immense appétit !
#+END_VERSE
#+END_CENTER

* Déjeuner sur couette

#+BEGIN_CENTER
#+BEGIN_VERSE
    Apéro, le champagne, effervescent d’attente,
    Agrémenté des fruits réveurs des océans
    Et puis quelques croissants d’une lune couchante
    Etoilés d’insomnies et d’espoir vascillant.

    De la langue en entrée, parfumée au gingembre
    Sous quelques fins cheveux embaumés de jasmin,
    Des cils aux doux accents de thym, de romarin
    Et des yeux affolés à brûler un décembre.

    Vient le rôti d’amour, légèrement saignant,
    Rosé comme ses joues, tendre comme ses seins,
    Nappé de doux soupirs, de poivre et de safran
    Et flambé de désir explosif et sans fin.

    Pour arroser le tout, le vin de la passion,
    Rouge comme sa robe, ardent comme ses reins,
    La grisante saveur des raisins sans pépins
    A consommer sur place et sans modération.

    Le dessert, aigre-doux, sorbet de jalousie
    Orage etincelé sur pluie de larmelettes
    Puis se calment tonnerre et éclairs de folie
    Lorsque le calumet met le feu sous la couette.
#+END_VERSE
#+END_CENTER

* Il faut tout brûler

#+BEGIN_CENTER
#+BEGIN_VERSE
    Il faut tout brûler, lui disais-je,
    Incendier tout ces vieux fantômes,
    Ces airs connus, ces vieux arpèges,
    Qui nous écrasent sous leur dôme.

    Au feu les photos du passé,
    Toutes ces ruines balayées,
    Ces visages emprisonnés
    Par le vent des heures glacées.

    Qu’il ne reste rien des instants
    Meublés de rires et de voix
    Qui encombrent nos sentiments
    De trop de poussiéreux éclats.

    Loin ces mines affriolantes,
    Vermine de notre raison,
    Que les flammes déliquescentes
    Les avalent de leur passion,

    Que leur fascinante colère
    Se répande dans nos esprits,
    Qu’ils sillonnent de leur lumière
    Ces cadres creux et décrépits

    Que le feu reforge nos coeurs
    De délires incandescents,
    D’appétits flambant de fureur
    Et de leurs désirs bouillonants.
#+END_VERSE
#+END_CENTER

* L'Eau de Feu

#+BEGIN_CENTER
#+BEGIN_VERSE
    Mon esprit vagabond flottait dans les éthers
    Que l’on trouve parfois au plus profond d’un verre
    Quand, de sa voix ambrée, mon Cognac demanda,
    Trémolo apaisant et ardent à la fois :

    Dis-moi vieux compagnon, saurais-tu par hasard
    D’où je viens ? Non ? Alors écoute cette histoire,
    Il s’agit du Feu et de l'Eau
    Qui, si fidèle est ma mémoire,
    Eurent un jour ces mots ...

    L'Eau :

    Tu brûles ! Doucement, tempère tes ardeurs ;
    Ton âtre incandescent me donne des vapeurs !
    Mon sang s’en va bouillir, je perle de rosée ,
    Tu y mets trop d’entrain, je me sens embrumée.

    Le Feu :

    Tes embruns sont glacés, j'étouffe sous leur poids,
    Veux-tu donc me briser et causer mon trépas ?
    Le désir me submerge et mes braises s’essoufflent,
    Vas-tu te refuser jusqu'à mon dernier souffle ?

    L'Eau :

    Ralentis je te dis, je ne suis que rivière,
    Devant un océan de baisers c’est bien peu ;
    Desserre ton étreinte, éteinds cette colère !
    Tu sais bien que j’accours seulement pour tes yeux.

    Le Feu :

    Vraiment c’en est assez, je n’obtiens que tes rives
    Quand vas-tu me laisser enfin passer à gué ?
    Je veux explorer tout de tes courbes lascives,
    Laisse la flamme enfin sur l’onde se coucher !

    L'Eau :

    Je sens monter en moi ces ardents tourbillons,
    Ces alambics d’où sort l'élixir de folie ;
    Mes galets caressants attisent ces démons
    Vois je suis comme toi : dévorée par l’envie !

    Le Feu :

    Réchauffe-moi ce lit, mes flammes s’y enfument !
    Ta bise me tisonne, un délire me gagne ;
    Sois fougueuse, mon Eau, non pas étang qui stagne
    Libère de tes flots ta frémissante écume !

    L'Eau :

    J’aspire à ta peau lisse, ôte-moi cette soie !

    Le Feu :

    Ne vois-tu pas déjà mes bûches qui rougeoient ?

    L'Eau :

    Mais qu’attends-tu lambin pour venir m’embraser !

    Le Feu :

    Arrachons sans détour tes atours enneigés !

    L'Eau et le Feu :

    Nous aimer c’est nous détruire ;
    Allons, il faut en finir !

    Tu l’auras deviné, ce n’est pas un mystère,
    Je suis le fruit de leur union tumultueuse
    Tout comme les Liqueurs et l'Armagnac mon frère,
    Sens-tu vibrer en moi leur fusion amoureuse ?
#+END_VERSE
#+END_CENTER

* Baisers incisifs

#+BEGIN_CENTER
#+BEGIN_VERSE
    Ton rire est chaleureux et ta mine engageante,
    Pourtant tes cils battants me laissent deviner,
    Derrière ce tableau de courbes enivrantes,
    Tes deux yeux prédateurs, superbes et glacés.

    Ton voile diaphane est félin et fruité
    Mais je sais que ton charme indécent dissimule
    Quelque sauvage croc de fauve carnassier
    Dont l'échine frissonne au seuil du crépuscule.

    Là où d’autres ne voient que du bleu et de l’or
    Rivière de cheveux, fine dentelle d’ambre,
    Tes nomades soupirs n’effleurent que mon corps :
    Mon âme est plus volcans que glaciers de décembre.

    Mais ce n’est rien, dévoile-moi tous tes arpèges :
    Il nous faut nous aimer avant de nous haïr,
    Ainsi le veut la Loi. Et puisque c’est un piège,
    Semons cet ouragan qui viendra nous détruire.

    Chante-moi ces airs faux, ces factices solfèges,
    Comme le veut l'Amour, je m’en vais y plonger
    Et je joindrai aux tiens mes propres sortilèges ;
    Mordons-nous chère amie, nos coeurs doivent saigner !

    Mélangeons nos poisons, que la fièvre nous gagne,
    Et nous mène au-delà des plus hautes montagnes !
    Echangeons nos venins, ma complice Vipère :
    Que ce soit une orgie de cocktail délétère !
#+END_VERSE
#+END_CENTER

* Lagune

#+BEGIN_CENTER
#+BEGIN_VERSE
    Au nom de tous ces mots qui n’ont pas été dits,
    Englués près de l’oeuf sans oser s’envoler
    Ou qui prirent l’envol et se sont fracassés
    Sur la falaise en grès du silence maudit,
    Au nom de ces griffons trempés dans le métal,
    Ricanements vautours des oisillons meurtris
    Au bec tranchant à vif dans la chair du moral,
    Au nom de ces gosiers d’où ne sort plus qu’un râle,
    Joignons nos lèvres pour partager un soupir,
    Sur cette plage d’ocre où dort l’or du désir.

    Au nom de ces regards qui sont restés au nid
    Douillet d’une paupière et d’un cil empaillé,
    Pris au piège feutré d’un globe détourné
    Et qui s'éteignent tous dans leurs sombres réduits,
    Pour ces braises noyées dans un lac lacrymal
    Que quelqu’esprit hargneux fait jaillir de l’oubli,
    Fantôme assoiffé de désespoir minéral,
    Au nom de ces iris noyés dans un canal,
    Eclairons nos yeux aux chandelles d’un sourire,
    Sur cette plage d’ocre où dort l’or du désir.

    Au nom de tous ces doigts qui se sont engourdis,
    Givrés dans les gants blancs d’un manège gelé,
    Figés par les reflets d’un passé momifié
    Dans la routine exsangue où le temps ralentit,
    Au nom de ces mains dont la lueur sidérale
    Ne se rappelle plus les gestes assoupis
    Dont elles se chauffaient au soleil matinal,
    Au nom de cette peau devenue froide et pâle,
    Brûlons-nous dans l'âtre de nos corps qui s’attirent,
    Sur cette plage d’ocre où dort l’or du désir.
#+END_VERSE
#+END_CENTER

* Essence

#+BEGIN_CENTER
#+BEGIN_VERSE
    Je vais volant sans fin, avide et dévorant,
    Semant la destruction, brisant ce qui se dresse,
    Tout n’est plus après moi que larmes de détresse
    Dont ma gorge brûlante se rit en buvant.

    As-tu déjà senti les caresses cuisantes
    Que prodiguent sans cesse mes lèvres luisantes ?

    Rassasié j’ai l’air calme et je dors sans ronfler
    Et l’on peut même alors tenter de m’asphyxier ;
    Mais je conseille au fol qui tenterait sa chance
    De ne pas trembloter : ce serait imprudence !

    As-tu déjà gouté les caresses cuisantes
    Que prodiguent sans cesse mes lèvres luisantes ?

    Les plus épais des murs ne font que m’aiguiser,
    Mes assauts turbulents les font tous frissonner ;
    Chacun d’eux tour a tour me cèdera son or,
    Je n’ai soif que de sang, de passion, de trésors ;

    Je te sens avide des caresses cuisantes
    Que prodiguent sans cesse mes lèvres luisantes !

    Ma soeur même me craint et voudrait m’enterrer,
    Devant moi l’on s’incline, on rampe ou l’on s’efface.
    Es-tu d’acier trempé, d’airain ou bien de glace ?
    Je te consumerai, ou bien je m'éteindrai !

    Je suis en toi mortel, ô futur tas de cendres !
    Grâce à moi tu survis s’il gèle à pierre fendre ;
    Grâce à moi tu aimes, désires et festoies,
    Je consume ton coeur, et tes reins et ton foie ;

    Pourrais-tu te passer des caresses cuisantes
    Que prodiguent sans cesse mes lèvres luisantes ?

    Pauvre mortel, ho non ! jamais tu ne pourras :
    Je suis le Feu, je suis ton âme, je suis Toi.
#+END_VERSE
#+END_CENTER



[[../index.php][Accueil]]

