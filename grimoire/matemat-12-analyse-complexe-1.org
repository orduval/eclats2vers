
#+STARTUP: showall

#+TITLE: Eclats de vers : Matemat 12 : Analyse complexe - 1
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

#+INCLUDE: "../include/commandes-tex.org"

* Exponentielle complexe

#+TOC: headlines 1 local

\label{chap:expocomp}


** Dépendances


  - Chapitre \ref{chap:complexe} : Les complexes



** Introduction

AFAIRE : ARRANGER LE CHAPITRE

Considérons l'unique solution $x$ de l'équation différentielle :

#+BEGIN_CENTER
\(
\OD{x}{t}(t) = z \cdot x(t) \\
x(0) = 1
\)
#+END_CENTER

où $z \in \setC$. On définit alors l'exponentielle d'un nombre complexe par :

$$\exp( z \cdot t ) = x(t)$$

On a donc simplement :

$$\exp(z) = x(1)$$


** Additivité

Comme les fonctions :

#+BEGIN_CENTER
\(
s(t) = \exp( (z_1 + z_2) \cdot t ) \\
p(t) = \exp(z_1 t) \cdot \exp(z_2 t)
\)
#+END_CENTER

vérifient la même équation différentielle :

#+BEGIN_CENTER
\(
\OD{s}{t}(t) = (z_1 + z_2) \ s(t) \\
s(0) = 1
\)
#+END_CENTER

#+BEGIN_CENTER
\(
\OD{p}{t}(t) = z_1 \ p(t) + z_2 \ p(t) = (z_1 + z_2) \ p(t) \\
p(0) = 1
\)
#+END_CENTER

on a $s(t) = p(t)$ pour tout $t$. En considérant le cas $t=1$, on arrive à
la propriété d'additivité des exponentielles :

$$\exp(z_1 + z_2) = \exp(z_1) \cdot \exp(z_2)$$


** Lien avec les fonctions trigonométriques

Considérons maintenant le cas particulier où $u(t) = \exp(\img t)$ :

#+BEGIN_CENTER
\(
\OD{u}{t}(t) = \img u(t) \\
u(0) = 1
\)
#+END_CENTER

On a alors :

$$\frac{d^2 u}{dt^2}(t) = - u(t) \qquad u(0) = 1 \qquad \OD{u}{t}(0) = \img$$

Soit la fonction $u : \setR \mapsto \setR$ définie pour tout réel $t$ par :

$$u(t) = \cos(t) + \img \sin(t)$$

On voit que :

$$\OD{u}{t} = -\sin(t) + \img \cos(t)$$

vérifie la même équation. Par unicité de la solution, on a :

$$\exp(\img t) = \cos(t) + \img \sin(t)$$

On en déduit directement que :

#+BEGIN_CENTER
\(
\exp(\img \pi/2) = \img \\
\exp(\img \pi) = -1 \\
\exp(3\pi\img/2) = \img \\
\exp(2\pi\img) = 1
\)
#+END_CENTER

ainsi que la périodicité :

$$\exp(t+2\pi) = \exp(t)$$

pour tout $t\in\setR$.

En utilisant l'additivité, on note que :

$$\exp(z) = \exp(a) \cdot \exp(\img b) =  \exp(a) \cdot (\cos(b) + \img \sin(b))$$

De la même façon, si nous prenons $u(t) = \exp(-i.t)$, nous en déduisons :

#+BEGIN_CENTER
\(
\frac{d^2 u}{dt^2}(t) = - u(t) \\
u(0) = 1 \qquad \OD{u}{t}(0) = -i
\)
#+END_CENTER

Donc :

$$\exp(-\img t) = \cos(t) - \img \sin(t)$$

En additionnant, puis en soustrayant les relations :

#+BEGIN_CENTER
\(
\exp( \img u ) = \cos(u) + \img \sin(u) \\
\exp( - \img u ) = \cos(u) - \img \sin(u)
\)
#+END_CENTER

on obtient :

#+BEGIN_CENTER
\(
\cos(u) = \frac{ \exp(\img u) + \exp(-\img u) }{2} \\
\sin(u) = \frac{ \exp(\img u) - \exp(-\img u) }{2 \img}
\)
#+END_CENTER

Choisissant un angle $\theta\in\setR$ tel que :

#+BEGIN_CENTER
\(
\cos(\theta) = \frac{\Re(z)}{\abs{z}} \\
\sin(\theta) = \frac{\Im(z)}{\abs{z}}
\)
#+END_CENTER

on peut réexprimer $z$ comme :

$$z = \abs{z}(\cos(\theta)+\img\sin(\theta)) = \abs{z}\exp(\img\theta)$$

Comme les fonctions $\cos$ sont $2\pi$ périodiques, il existe une infinité
d'angles $\theta$ vérifiant cette propriété. On définit l'argument de
$z$ comme l'unique $\theta$ vérifiant cette propriété et se trouvant
dans l'intervalle $[0,2\pi)$.

#+BEGIN_CENTER
\(
\theta = \arg(z) \quad\Leftrightarrow\quad
\begin{cases}
z = \abs{z}\exp(\img\theta) \\
\theta\in [0,2\pi)
\end{cases}
\)
#+END_CENTER

Inspiré par relation :

$$z = \abs{z}\exp(\img\arg(z))$$

et cherchant à étendre la propriété :

$$\ln(x \cdot y) = \ln(x) + \ln(y)$$

du logarithme sur $\setR$, on définit le logarithme d'un complexe par :

#+BEGIN_CENTER
\(
\ln(z) = \ln(\abs{z}) + \ln(\exp(\img\arg(z))) \\
\ln(z) = \ln(\abs{z}) + \img\arg(z)
\)
#+END_CENTER

Pour un $z \in \setC$ donné, l'ensemble des $y \in \setC$ tels que :

$$\exp(y) = z$$

peut s'écrire :

$$\mathcal{Y} = \{ y_k = \ln(\abs{z}) + \img \arg(z) + 2 \pi \img k : k \in \setZ \}$$

Les complexes permettent de retrouver aisément les propriétés
fondamentales des fonctions trigonométriques $\cos$, $\sin$.
En se rappelant que :

$$\exp(-\img t) \cdot \exp(i t) = \exp(i t - i t) = \exp(0) = 1$$

on retombe sur :

#+BEGIN_CENTER
\(
( \cos(t) + \img \sin(t) ) ( \cos(t) - \img \sin(t) ) = 1 \\
\cos(t)^2 + \sin(t)^2 = 1
\)
#+END_CENTER

En dérivant la relation reliant l'exponentielle aux fonctions trigonométriques, on obtient :

#+BEGIN_CENTER
\(
\OD{}{t}( \cos(t) + \img \sin(t) ) = \OD{}{t}\exp(\img t) \\
\OD{}{t} \cos(t) + \img \OD{}{t}\sin(t) = \img \exp(\img t) \\
\OD{}{t} \cos(t) + \img \OD{}{t}\sin(t) = \img \cos(t) - \sin(t)
\)
#+END_CENTER

on a bien :

#+BEGIN_CENTER
\(
\OD{}{t} \cos(t) = -\sin(t) \\
\OD{}{t}\sin(t) = \cos(t)
\)
#+END_CENTER

On a aussi :

#+BEGIN_CENTER
\(
\cos(u+v) + \img \sin(u+v) = \exp\left[ \img ( u + v ) \right] \\
\cos(u+v) + \img \sin(u+v) = \exp\left[ \img u  \right] \cdot \exp\left[ \img  v \right] \\
\cos(u+v) + \img \sin(u+v) = (\cos(u) + \img \sin(u))(\cos(v) + \img \sin(v))
\)
#+END_CENTER

ce qui donne, tous calculs faits :

#+BEGIN_CENTER
\(
\cos(u+v) = \cos(u)\cos(v) - \sin(u)\sin(v) \\
\sin(u+v) = \sin(u)\cos(v) + \sin(v)\cos(u)
\)
#+END_CENTER


* Dérivation et intégration dans le plan complexe

#+TOC: headlines 1 local

\label{chap:dicplx}


** Dépendances

  - Chapitre \ref{chap:differ} : Les différentielles
  - Chapitre \ref{chap:integ} : Les intégrales


** Dérivées

On définit la dérivée d'une fonction $f : \setC \mapsto \setC$
par analogie avec la dérivée des fonctions réelles :

$$\OD{f}{z}(z) = \lim_{ \substack{ h \to 0 \\ h \in\setC } } \frac{f(z+h) - f(z)}{h}$$

Si $f$ est dérivable sur $A\subseteq\setC$, on dit qu'elle est analytique
sur $A$.

On définit les fonctions $u,v : \setR^2 \mapsto \setR$ associées à $f$ par :

#+BEGIN_CENTER
\(
u(x,y) = \Re\left(f(x + \img y) \right) \\
v(x,y) = \Im\left(f(x + \img y) \right)
\)
#+END_CENTER

On a alors $f(z) = u(x,y) + \img v(x,y)$. Si $f$ est dérivable, la limite
ne peut pas dépendre du chemin suivi pour arriver en $(x,y)$. On peut donc
choisir $h$ de la forme $\Delta x$ ou $\img \Delta y$ avec
$\Delta x,\Delta y\in\setR$. Cela donne :

#+BEGIN_CENTER
\(
\OD{f}{z}(z) = \lim_{\Delta x \to 0}
\frac{\PD{u}{x}\Delta x + \img \PD{v}{x}\Delta x}{\Delta x} \\
\OD{f}{z}(z) = \lim_{\Delta y \to 0}
\frac{\PD{u}{y}\Delta y + \img \PD{v}{y}\Delta y}{\img \Delta y}
\)
#+END_CENTER

et par comparaison :

#+BEGIN_CENTER
\(
\PD{u}{x} = \PD{v}{y} \\
\PD{u}{y} = -\PD{v}{x}
\)
#+END_CENTER

En dérivant de nouveau ces équations, on obtient :

#+BEGIN_CENTER
\(
\frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2} = 0 \\
\frac{\partial^2 v}{\partial x^2} + \frac{\partial^2 v}{\partial y^2} = 0
\)
#+END_CENTER

Les puissances de $z$ sont dérivables. On vérifie que :

$$\OD{z^n}{z}=n z^{n-1}$$


** Extension de l'intégrale au plan complexe

Soit une fonction $f : A \mapsto \setC$. Comme
$u = \Re(f)$ et $v = \Im(f)$ sont des fonctions à valeurs réelles :

$$u,v : A \mapsto \setR$$

leur intégrale est bien définie. On définit alors l'intégrale de $f$ par :

$$\int_A f d\mu = \int_A u d\mu + \img \int_A v d\mu$$


** Intégrale de ligne

Soit une fonction $\gamma : [a,b] \mapsto \setC$ et la courbe :

$$l = \{ \gamma(t) : t \in [a,b] \} \subset \setC$$

On définit l'intégrale de ligne de $f$ sur $l$ par :

$$\int_l f(z) dz = \int_a^b (f \circ \gamma)(t) \OD{\gamma}{t}(t) dt$$

On peut également écrire :

#+BEGIN_CENTER
\(
\int_l f(z) dz = \int_l (u + \img v)(dx + \img dy) \\
\int_l f(z) dz = \int_l (u dx - v dy) + \img \int_l (u dy + v dx)
\)
#+END_CENTER

où les intégrales des membres de droites sont des intégrales de ligne classiques
de fonctions $\setR^2 \mapsto \setR$.


*** Théorème fondamental

Soit une fonction analytique $F : \setC \mapsto \setC$. La dérivée
d'une composée de fonction nous donne :

$$\OD{}{t}(F \circ \gamma)(t) = (\OD{F}{z} \circ \gamma)(t) \OD{\gamma}{t}(t)$$

On en déduit, en appliquant le théorème fondamental que :

$$\int_l \OD{F}{z}(z) dz = F(\gamma(b))-F(\gamma(a))$$


*** Contour fermé

Dans la suite, lorsque $l$ est une courbe fermée et est donc la frontière d'un
certain ensemble $l = \partial S$ inclus dans $\setC$, nous notons :

$$\oint_l f(z) dz = \int_{\partial S} f(z) dz$$

Soit $f$ une fonction analytique et $l = \partial S$ une courbe fermée. On a :

$$\oint_l f(z) dz = \oint_l (u dx - v dy) + \img \oint_l (u dy + v dx)$$

En appliquant le théorème de Stokes, il vient :

#+BEGIN_CENTER
\(
\oint_l f(z) dz = \int_S (-\PD{v}{x}-\PD{u}{y}) dS + \img \int_S (\PD{u}{x}-\PD{v}{y}) dS \\
\)
#+END_CENTER

Mais comme $f$ est analytique :

#+BEGIN_CENTER
\(
\PD{u}{x} = \PD{v}{y} \\
\PD{u}{y} = -\PD{v}{x}
\)
#+END_CENTER

Les termes du membre de droite s'annulent donc et :


$$\oint_l f(z) dz = 0$$


*** Cercle

Soit $C$ le cercle de centre $a$ et de rayon $R$ :

$$C = \{ \gamma(\theta) = a + R \exp(\img\theta) : \theta \in [ 0 , 2 \pi ) \}$$

On a alors :

$$\OD{\gamma}{\theta}(\theta) = R \img \exp(\img\theta)$$

et :

$$(z - a)^k = \left( R \exp(\img\theta) \right)^k$$

pour tout $z\in C$ et $k \in \setZ$. Donc :

#+BEGIN_CENTER
\(
\oint_C \frac{dz}{(z-a)^k} =
\int_0^{2\pi} \frac{R \img \exp(\img\theta)}{R^k \exp(\img k\theta)} \\
\oint_C \frac{dz}{(z-a)^k} = \img R^{1-k} \int_0^{2\pi} \exp[\img(1-k)\theta] d\theta
\)
#+END_CENTER

Mais les propriétés de périodicité des fonctions trigonométriques et de l'exponentielle
associée nous permettent de vérifier que :

$$\int_0^{2\pi} \exp[\img l \theta] d\theta = 2 \pi \delta_{l,0}$$

pour tout $l\in\setZ$. On a donc :

$$\oint_C \frac{dz}{(z-a)^k} = 2 \pi \img \delta_{k,1}$$

pour tout $k \in \setZ$. L'intégrale s'annule pour tout les $k \ne 1$.

Vu que la fonction $f(z) = (z-a)^{-k}$ est analytique partout sauf en $a$,
on obtient le même résultat pour toute courbe fermée entourant $a$ :


$$\oint_{\partial S} \frac{dz}{(z-a)^k} = 2 \pi \img \delta_{k,1} \delta_S(a)$$


*** Théorème de Cauchy

Si $f$ est analytique, la fonction

#+BEGIN_CENTER
\(
g(z) =
\begin{cases}
\frac{f(z)-f(a)}{z-a} & \mbox{si  } z \ne a \\
\OD{f}{z}(a) & \mbox{si  } z = a
\end{cases}
\)
#+END_CENTER

est analytique aussi. On a donc :

$$\oint_l g(z) dz = 0$$

Soit $a\in\setC$ et une courbe fermée entourant $a$ telle que $a\notin l$. On a alors :

$$\oint_l \frac{f(z)-f(a)}{z-a} dz = \oint_l g(z) dz = 0$$

On en déduit que :

$$f(a) \oint_{\partial S} \frac{1}{z-a} dz  = \oint_{\partial S} \frac{f(z)}{z-a} dz$$

ce qui nous donne la valeur de $f(a)$ en fonction d'une intégrale :

$$f(a)  = \frac{1}{2 \pi \img} \oint_{\partial S} \frac{f(z)}{z-a} dz$$

En dérivant $k$ fois cette dernière relation par rapport à $a$, on obtient :

$$\OD{f}{z}(a) = \frac{k !}{2 \pi \img} \oint_{\partial S} \frac{f(z)}{(z-a)^{k+1}} dz$$


*** Séries de Laurent

Supposons que $f$ puisse s'écrire comme une combinaison linéaire de puissances
entière de $z-a$. On a :

$$f(z) = \sum_{n=-\infty}^{+\infty} \alpha_n (z-a)^n$$

Multipliant le tout par $(z-a)^{-k-1}$ et intégrant sur un contour entourant $a$,
on obtient :

$$\oint_l f(z)(z-a)^{-k-1} dz = \sum_{n=-\infty}^{+\infty} \alpha_n \oint_l (z-a)^{n-k-1} dz$$

Mais nous avons vu que seule l'intégrale de $(z-a)^{-1}$ ne s'annule pas. On en déduit que :

$$\alpha_k = \frac{1}{2 \pi \img} \oint_l \frac{f(z)}{(z-a)^{k+1}} dz$$


*** Théorème des résidus

Considérons le cas où :

$$f(z) = \sum_{n=-p}^{+\infty} \alpha_n (z-a)^n$$

pour un certain $p \in \setZ$. On dit alors que $f$ à un pôle d'ordre $p$ en $a$.
En multpliant le tout par $(z-a)^p$ et en dérivant $p-1$ fois, on obtient :

$$\frac{d^{p-1}}{dz^{p-1}}[(z-a)^p f(z)] = (p-1) ! \left[a_{-1} + a_0 (z-a) + a_1 (z-a)^2 + ...\right]$$

On a donc :

$$\lim_{z \to a} \frac{d^{p-1}}{dz^{p-1}}[(z-a)^p f(z)] = (p-1) ! a_{-1}$$

Mais comme $a_{-1}$ n'est rien d'autre, à un facteur $2\pi\img$ près, que l'intégrale de
$f$ sur un contour entourant $a$, on a :
$$$$
\oint_l f(z) dz = 2 \pi \img \lim_{z \to a} \frac{1}{(p-1)!} \frac{d^{p-1}}{dz^{p-1}}[(z-a)^p f(z)]


* Polynômes et exponentielles

#+TOC: headlines 1 local

\label{chap:polyexpo}


** Chebyshev

Les polynômes de Chebyshev $T_n$ sont défini par :

#+BEGIN_CENTER
\(
T_n : x\mapsto\cos(n \arccos(x) ) \\
T_n(\cos(\theta)) = \cos(n \theta)
\)
#+END_CENTER

Il est clair que :

#+BEGIN_CENTER
\(
T_0(x) = 1 \\
T_1(x) = x
\)
#+END_CENTER

Les propriétés des fonctions trigonométriques :

#+BEGIN_CENTER
\(
\cos[(n+m)\theta]= \cos[n\theta]\cos[m\theta]+\sin[n\theta]\sin[m\theta] \\
\cos[(n-m)\theta]= \cos[n\theta]\cos[m\theta]-\sin[n\theta]\sin[m\theta]
\)
#+END_CENTER

nous montrent que :

$$T_{n+m}(x) + T_{n-m}(x) = 2 T_n(x) T_m(x)$$

On en déduit entre autre que :

$$T_{n+1}(x) + T_{n-1}(x) = 2 x T_n(x)$$

Les fonctions $T_n$ sont donc des polynômes. Comme :

$$T_n \left[ \cos\left(\frac{2k+1}{2n}\right) \right] = 0$$

les racines sont données par :

$$x_k = \cos\left(\frac{2k+1}{2n}\right)$$

pour $k=0,1,...,n-1$.
On déduit aussi la propriété suivante de la définition :

$$(T_n \circ T_m)(x) = T_{m \cdot n}(x) = (T_m \circ T_n)(x)$$

Nous allons voir que les $T_n$ sont orthogonaux moyennant un certain
poids. Si $m = n = 0$, il est clair que :

$$\int_0^\pi T_0(\cos(\theta)) T_0(\cos(\theta)) d\theta = \int_0^\pi d\theta = \pi$$

Si $m, n \ne 0$, on a :

#+BEGIN_CENTER
\(
\int_0^\pi T_m(\cos(\theta)) T_n(\cos(\theta)) d\theta =
\int_0^\pi \cos(m\theta) \cos(n\theta) d\theta \\
\int_0^\pi T_m(\cos(\theta)) T_n(\cos(\theta)) d\theta =
\unsur{2} \int_0^\pi \cos[(m+n)\theta] d\theta +
\unsur{2} \int_0^\pi \cos[(m-n)\theta] d\theta
\)
#+END_CENTER

Si $m = n$, on a alors :

$$\int_0^\pi T_m(\cos(\theta)) T_n(\cos(\theta)) d\theta = \unsur{2n} [\sin(2 n \pi) - \sin(0)] + \frac{\pi}{2}$$

La première intégrale s'annule donc par périodicité de la fonction
$\sin$. A présent, si $m \ne n$, on a :

#+BEGIN_CENTER
\(
\int_0^\pi T_m(\cos(\theta)) T_n(\cos(\theta)) d\theta = \\
\unsur{2(m+n)} [\sin[(m+n)\pi] - \sin(0)] + \\
\unsur{2(m-n)} [\sin[(m-n)\pi] - \sin(0)] = 0
\)
#+END_CENTER

Rassemblant ces résultats, on obtient :

#+BEGIN_CENTER
\(
\int_0^\pi T_m(\cos\theta) T_n(\cos\theta) d\theta =
\begin{cases}
\pi & \mbox{si  } m = 0 \\
\pi/2 & \mbox{si  } m = n \ne 0 \\
0 & \mbox{si  } m \ne n
\end{cases}
\)
#+END_CENTER

Le changement de variable $x = \cos(\theta)$,

#+BEGIN_CENTER
\(
dx = -\sin(\theta) d\theta \\
d\theta = \frac{dx}{\sqrt{1-x^2}}
\)
#+END_CENTER

nous donne :

$$\int_0^\pi T_m(\cos\theta) T_n(\cos\theta) d\theta = \int_{-1}^1 \frac{T_m(x) T_n(x)}{\sqrt{1-x^2}}dx$$

On en déduit que :

$$\int_{-1}^1 \frac{T_m(x) T_n(x)}{\sqrt{1-x^2}} dx = \delta_{mn} \frac{\pi}{2} (1+\delta_{m,0})$$

pour tout $m,n\in\setN$.


** Hermite

Les polynômes de Hermite sont orthogonaux pour le produit scalaire :

$$\int_{-\infty}^{+\infty} H_n(x) H_m(x) \exp(-x^2) dx = 2^n n ! \sqrt{\pi} \delta_{mn}$$

Ils obéissent à la récurrence :

#+BEGIN_CENTER
\(
H_0(x) = 1 \\
H_1(x) = 2 x \\
H_{n+1}(x) = 2 x H_n(x) - 2 n H_{n-1}(x) \\
\)
#+END_CENTER


** Laguerre

Les polynômes de Laguerre sont orthogonaux pour le produit scalaire :

$$\int_{0}^{+\infty} L_n(x) L_m(x) \exp(-x) dx = \delta_{mn}$$

Ils obéissent à la récurrence :

#+BEGIN_CENTER
\(
L_0(x) = 1 \\
L_1(x) = 1-x \\
(n+1) L_{n+1}(x) = (2 n + 1 - x) L_n(x) - n L_{n-1}(x)
\)
#+END_CENTER
