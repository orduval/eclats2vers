
#+STARTUP: showall

#+TITLE: Eclats de vers : Opus : Musica : Template : Mélodie
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+INCLUDE: "../../../include/navigan-1.org"

#+TOC: headlines 1

#+TAGS: noexport(n)

* Eclats de vers: Opus : Musica : template : mélodie

#+TOC: headlines 1 local

** Mesure 3/4, carrure 2 x 4

#+include: "~/racine/musica/lilypond/template/melody/mel-3-2-4.mld.ly" src lilypond

** Mesure 4/4, carrure 2 x 4

#+include: "~/racine/musica/lilypond/template/melody/mel-4-2-4.mld.ly" src lilypond

** Mesure 2/2, carrure 2 x 4

#+include: "~/racine/musica/lilypond/template/melody/mel-2-2-4.mld.ly" src lilypond

** Mesure 4/4, carrure 3 x 2

#+include: "~/racine/musica/lilypond/template/melody/mel-4-3-2.mld.ly" src lilypond

** Mesure 2/2, carrure 3 x 2

#+include: "~/racine/musica/lilypond/template/melody/mel-2-3-2.mld.ly" src lilypond

** Mesure 3/4, carrure 3 x 2

#+include: "~/racine/musica/lilypond/template/melody/mel-3-3-2.mld.ly" src lilypond
