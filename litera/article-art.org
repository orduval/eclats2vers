#+STARTUP: showall

#+TITLE: Eclats de vers : Article : Art
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index littéraire]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Ces fausses vraies vulgarités

Ce n'est un secret pour personne, l'être humain est porté sur la chose.
Et cela est fort heureux, car dans le cas contraire nous ne serions pas
là pour en témoigner. Mais quelle chose, me direz-vous ? Le coeur du
problème est bien là : malgré le vif intérêt que nous lui portons, ce
point vers lequel convergent nos émotions les plus intenses est
socialement soumis à la désapprobation publique. Raison pour laquelle
nous en parlons souvent en code ou à demi-mots. Ne le nions pas, il y a
là-dedans une pincée d'hypocrisie. Mais pincée n'est ni seau ni pinceau,
et il en va de l'hypocrisie comme de ces fines épices : indigeste en
surabondance, elle devient des plus savoureuses lorsqu'elle est dégustée
à petites doses de sous-entendus. Rentrons plus avant dans le vif du
sujet, avec Louise Labé qui, au 16e siècle, clamait haut et fort :

#+BEGIN_CENTER
#+BEGIN_VERSE
    Baise m’encor, rebaise-moi et baise ;
    Donne m’en un de tes plus savoureux,
    Donne m’en un de tes plus amoureux :
    Je t’en rendrai quatre plus chauds que braise.
    ...
#+END_VERSE
#+END_CENTER

Contrairement aux apparences, elle ne réclamait pas par là publiquement
les assauts fougueux de son amant, le verbe baiser signifiant
simplement, d'après le Trésor :

Baiser : Effleurer, toucher de ses lèvres quelque partie d'une personne
(surtout la main, la joue) ou quelque objet la symbolisant.

La main et la joue, pourquoi pas, c'est un début après tout. Quoiqu'il
en soit, le double sens libidineux de ce genre de galanterie semble
n'avoir dupé personne, puisqu'il a débouché sur le sens populaire que
l'on connaît actuellement. Villon n'est pas en reste, avec toutefois la
nuance d'une teinte bucolique puisqu'il s'agit d'un cours de jardinage :

#+BEGIN_CENTER
#+BEGIN_VERSE
    Ces larges rains, ce sadinet
    Assis sur grosses fermes cuisses,
    Dedans son petit jardinet
#+END_VERSE
#+END_CENTER

Bien entendu, le terme de /sadinet/ (du latin /sapidus/, savoureux,
l'homme était connaisseur semble-t-il) désigne ici le sexe féminin, un
champ sémantique que le marquis de Sade n'aurait pas renié. Morale de
l'histoire, il n'y a pas loin du champ au jardinet en passant par la
grange. Il n'y a pas loin non plus de la grange à la guérite dont
Baudelaire nous parle dans son « Sonnet d'automne » :

#+BEGIN_CENTER
#+BEGIN_VERSE
    Aimons-nous doucement. L'Amour dans sa guérite,
    Ténébreux, embusqué, bande son arc fatal.
    Je connais les engins de son vieil arsenal :
#+END_VERSE
#+END_CENTER

il n'avoue pas l'érection de son engin, mais bien que l'amour est à la
peine :

Bander : Tendre avec effort.

On peut bien entendu supposer maintes interprétations métaphoriques plus
ou moins emmenées à ce tercet. Toutefois, c'est le septième sens et le
plus osé que l'histoire a retenu. Et puisque l'on parle d'érection, que
l'on sache qu'il s'agit simplement de :

Erection : Action d'élever.

L'imagination laissant fièrement voltiger son drapeau au vent, la
signification populaire s'est une nouvelle fois embourbée dans les
traversins que l'on sait. Dans le même siècle, Musset se fait le chantre
de la danse, dans « À la mi-carême » :

#+BEGIN_CENTER
#+BEGIN_VERSE
    C’est alors que les bals, plus joyeux et plus rares,
    Prolongent plus longtemps leurs dernières fanfares ;
    À ce bruit qui nous quitte, on court avec ardeur ;
    La valseuse se livre avec plus de langueur :
    Les yeux sont plus hardis, les lèvres moins avares,
    La lassitude enivre, et l’amour vient au coeur.
#+END_VERSE
#+END_CENTER

Vous l'aurez compris, sa charmante valseuse n'a rien à voir avec la
bourse ! Plus tard cependant, la méta-fore houleuse s'imposera à nos
yeux ébahis. Je vous épargne le terme portuaire chanté par Renaud dans «
Le vent soufflera » :

#+BEGIN_CENTER
#+BEGIN_VERSE
    Assise sur une bitte
    D’amarrage, elle pleure
#+END_VERSE
#+END_CENTER

les verges flagellantes :

Verge : Baguette de bois longue, fine et flexible; tige de métal longue
et fine. Verge d'un arc, d'un fouet, ...

les cons qui ne sont plus ce qu'ils étaient :

Con : Région du corps féminin où aboutissent l'urètre et la vulve.

et tout le stupre dégoulinant d'une liste exhaustive. Sachez simplement
qu'il en va de même pour bon nombre de mots de notre langue, et ce
jusqu'aux plus usuels. A moins que vous ne soyez incorrigiblement
ingénu(e) (je n'ai pas dit singe et nu(e)), si je vous dis que :

#+BEGIN_CENTER
#+BEGIN_VERSE
    Les brebis nocturnes de Lesbos broutent le soir
    Lorsque les cruches s’en vont à l’eau au ruissellement des rondelles
    Je répète ...
    Les brebis nocturnes de Lesbos broutent le soir
    Lorsque les cruches s’en vont à l’eau au ruissellement des rondelles
    Trois fois (et plus si affinité)
#+END_VERSE
#+END_CENTER

vous irez sûrement penser qu'il y a là, savane-ment caché dans les
buissons, comme un message inavouable. Vous n'aurez pas tord. Je
terminerai par cette constatation toute simple :

Vulgaire : (Vieilli ou littér.) Qui est admis, pratiqué par la grande
majorité des personnes composant une collectivité, appartenant à une
culture; qui est répandu. Qui est identique, semblable aux autres
individus, aux autres objets de son espèce. Synon. anodin, banal,
ordinaire.

Il est vrai que la vulgarité est une chose bien ordinaire.

* Sur l'art poétique de Verlaine

Depuis longtemps cette question me rongeait : comment le roi de la
chanson grise, le prince de la poésie mélodieuse en était-il arrivé à
mépriser les ailes sur lesquelles il a si brillamment voltigé. Ces vers
semblent a priori sans appel :

#+BEGIN_CENTER
#+BEGIN_VERSE
    Ô qui dira les torts de la Rime ?
    Quel enfant sourd ou quel nègre fou
    Nous a forgé ce bijou d’un sou
    Qui sonne creux et faux sous la lime ?
#+END_VERSE
#+END_CENTER

Voilà qui peut sembler paradoxal : on n'écrit pas un art poétique
incrusté de rimes tout en accusant la pauvrette. D'autant plus que, dans
le même poème, la première strophe nous donne un tout autre son de
cloche :

#+BEGIN_CENTER
#+BEGIN_VERSE
    De la musique avant toute chose,
    Et pour cela préfère l'Impair
    Plus vague et plus soluble dans l’air,
    Sans rien en lui qui pèse ou qui pose.
#+END_VERSE
#+END_CENTER

Passons sur l'impair, et retenons-en l'essentiel, à savoir la musique,
profondément liée aux assonances, allitérations et à toute forme de rime
au sens large. Ces deux strophes se révélent des plus contradictoires.
Mais où veut-il en venir ? La réponse pourrait très bien se glisser dans
la rime justement, celle qui ajuste fou à sou. Il faut savoir qu'à
l'époque ce genre de rime pauvre (c'est en fait une simple assonance)
était des plus déconseillée. Tout juste tolérée, et encore. Le bijou
d'un sou serait alors le caractère excessivement rigide de la prosodie
de l'époque, gourmande en rimes riches. Il ne nous parle donc pas de
l'abolir mais de la laisser gambader dans un style beaucoup plus souple,
délayé dans le flou artistique, la musique avant la règle, l'esprit
avant la lettre. Ce n'est qu'une hypothèse bien sûr, mais qui s'ajuste
parfaitement.

* La poésie libre

Alors là pas question de faire l'impasse, c'est non, non et encore non !
Désolé que ça tombe sur toi Fairy Tales, n'y vois rien de personnel,
mais à force d'entendre toujours les mêmes rengaines, la coupe finit par
déborder de l'eau du vase et les plombs du cable sautent ! Surtout par
temps lourd, les muses prennent vite la mouche d'orage ... donc je dis
Non !

Non, la poésie versifiée n'est pas cette créature désséchée, décrépite,
apesantie, engoncée dans un corset qui la torture. C'est une jeune
donzelle qui danse et tourbillonne, une déesse qui suit le rythme parce
qu'elle sait que le rythme suit son coeur débordant de fougue, c'est une
grande dame qui se laisse aimer par le chant lancinant des vagues, cette
source d'art pur, sublime symbiose de régularité et de fantaisie, c'est
une reine qui se plie par plaisir aux caprices de la strophe et aux
morsures du poème.

Non, le fond n'est pas plus important que la forme, le rythme et la
coloration phonétique sont déterminants. Plus que cela, ils ont une
signification qui se superpose à celle du langage. Essayez de faire
décoller de la poésie non cadencée, je vous promets bien du plaisir.
Tous les poèmes réussis que j'ai pu lire ici ou ailleurs (le ici ou
ailleurs on s'en fout, c'est juste de la broderie mimi) ont un rythme
interne qui vous entraîne et empêche le texte de couler lamentablement.
En bref, il y faut du swing, ou prévoir des bouées, ou lire un roman le
soir, assommant de préférence, ça épargne les soporifiques.

À coté de cela, nous avons devant nous une poésie qui se prétend « libre
», une poésie qui prétend enfermer ses soeurs de lyre d'un sous-entendu
trop éloquent pour ne pas être vulgaire. Et vous voudriez sans doute
qu'on les laisse croupir au couvent de l'église surréaliste, victime de
la frigidité jalouse de quelques proses bien pâles ?

Je ne sais quel disciple imbibé de cette icône de Rimbaud a inventé
cette expression pléonasmique de « poésie libre », mais excusons-le, on
a vu ce que donne l'utilisation d'absinthe sur la voix de son maître.
Les « Illuminations » portent bien leur nom, laissons-leur cela. Je ne
sais pourquoi, ce titre me fait penser aux enluminures, vous savez ces
petites bandes dessinées médiévales. Avec ce brave Rimbaud, loubard
déifié par une postérité pour le moins postérieure, c'est encore plus
facile : comme personne n'y comprend rien, il suffit de faire semblant !
Les nombreux adeptes doivent apprécier ce geste touchant dont ils
drapent leur amour-propre.

Quoiqu'il en soit, la « poésie dite libre » ne l'est pas plus qu'une
autre, et même plutôt moins puisqu'écrire en vers libres revient
actuellement à se laisser porter par le courant. La voie de la facilité
qui évite de devoir apprendre, qui fait bien, qui fait rebelle, qui fait
moderne, qui fait post mai 68. Nous sommes tous des rebelles dans un
monde de rebelles, avec de la musique de rebelle, du café de rebelle,
des boulots de rebelle, des poubelles de rebelle, de la routine de
rebelle, de l'ennui de rebelle. Nous tombons tellement d'accord sur le
principe de la rebellion qu'il n'y a même plus besoin de se rebeller,
c'est-y pas magnifique ?

Mais revenons à mai 68 et ses pavés, mai 68 et ses feux de joie bariolés
d'arc-en-ciels flower power, mai 68 et ses gentils organisateurs
casqués, mai 68 et ses futurs bourgeois à l'eau de rose, mai 68 et ses
slogans faciles. Quand je vois une ineptie du style « interdit
d'interdire », je me demande si il y avait vraiment des intellectuels
sur les barricades. À croire que trop lire de philosophie doit finir par
vous embrouiller un esprit, le bon sens le plus élémentaire passe à la
trappe.

Quel mois romanesque, vraiment ! On lui doit cette vision déformée de la
liberté, cette hantise absurde de la contrainte, ce dénigrement du
savoir, et par corollaire de tout ce qui ne ressemble pas assez au chaos
idyllique dont rêvent tous les nostalgiques de l'obscurantisme et de la
barbarie de l'époque féodale. On lui doit ce culte de la nouveauté
recyclable servi à toutes les sauces lors des fréquentes éphémérides
cosmiques où paradent des cosmétiques qui puent, on lui doit ces modes
incessantes qui tournent en rond autour du néant absolu tout en
marmonnant une litanie dogmatique, ces amas de jeunes filles
squelettiques, la téléréalité siliconée, le grand complot mondial de
l'univers, et, pire que tout, la fin de la diffusion des Cousteau sur la
deux.

Assumez, Attilas en herbe qui foulez sous vos sabots boueux l'eau
limpide de cette rigueur qui clarifie l'âme lorsqu'on l'accouple à
l'intuition ! Cessez de respecter toute règle orthographique ou
grammaticale, inventez une langue libérée que vous serez seul à
comprendre, et surtout amusez-vous bien dans votre coin. D'ailleurs je
vous déconseille d'apprendre à écrire, ça risquerait d'étouffer votre
créativité.

Assumez, fanatiques de la nouveauté bourdonnante : n'empruntez plus
aucun véhicule à roue, c'est obsolète ça, la roue. Coupez le chauffage,
le principe date de l'antiquité, et puis vendez vos quatre murs, c'est
carrément trop antédiluvien le concept de mur.

Vous voulez de la liberté ? Vous venez d'en lire, iconoclaste et
provocatrice à souhait, à contre-courant de la bien-pensance c'est vrai,
mais par franchise, non par frime. La Liberté c'est se moquer du sens du
courant m'sieurs-dames, ce n'est ni le suivre pour faire plaisir au
mouton voisin, ni ramer à contre-sens pour se donner une illusion
d'indépendance.

Qu'on se le dise, il n'est plus question de subir les adorateurs
inconditionnels du vers à mètre aléatoire sans leur demander s'il leur
arrive de danser.

⁂

Pour préciser ma pensée, la forme possède un sens propre, un peu comme
ces musiques instrumentales qui font preuve d'une telle force de
suggestion qu'elles peuvent compléter voir remplacer les paroles ; il ne
s'agit donc pas de faire passer le fond en arrière-plan, mais au
contraire de l'étendre à la suggestion qui naît de l'aspect musical d'un
poème. Aragon a écrit une fort belle ballade à ce sujet d'ailleurs, dont
le refrain est :

#+BEGIN_CENTER
#+BEGIN_VERSE
    Ce sont paroles de Grenade
#+END_VERSE
#+END_CENTER

Il fait allusion à la musique arabo-andalouse du 15e siècle qui avait
d'après lui la particularité de ne pas être systèmatiquement complétée
par un chant, ce qui n'était pas évident comme concept à l'époque.

En ce qui concerne la pensée, il est vrai qu'elle devient vite
impressionniste en poésie, l'abondance du langage imagé, détourné du
sens commun, rendant parfois les interprétations compliquées, on peut
même jouer dessus afin de donner plusieurs sens à un texte. La netteté
du discours y est en fait un paramètre sur lequel l'auteur peut jouer.

Ceci dit, je te suis sur la nécessité d'une pensée claire et rigoureuse,
celle de la philosophie et de la science au sens large.

* Les langues

[i](combien de théorèmes, en mathématique par exemple, se contredisent
les uns les autres ? ce qui est par exemple applicable en géométrie
euclidienne ne l'est pas toujours en géométrie spatiale)[/i]

Aucun malheureux, où tout s'écroulerait ! Hérésie, blasphème,
sorcellerie ! ;) Les différents types de géométries sont des objets
différents, chacune a ses propres lois. Ce qui est valable dans l'une ne
l'est pas forcément dans l'autre, nulle contradiction là-dedans.

Pour en revenir aux langues, chacune a sa propre structure, son propre
timbre, son propre réseau reliant les mots les uns aux autre via le sens
ou la sonorité. Malgré tout, il existe des familles de langues proches
les unes des autres. La question sous-jacente est : existe-t-il un
langage universel dont toutes les langues découlent ? Ce qui nous mène à
la question annexe : existe-t-il des pensées/émotions universelles
intégrées dans toutes les langues, ou presque ? Si oui, comment les
langues intègrent-elles ces notions ?

J'aurais tendance à répondre oui à la première question. Par exemple,
l'univers semble construit sur des formes récurrentes comme la dualité
cercle / onde que l'on retrouve partout, de la mécanique quantique à
l'échelle cosmique.

Le cas de l'émotion me semble encore plus évident : il suffit d'observer
un nombre suffisant de personnes pour constater que des schémas
psychologiques récurrents apparaissent. Si certains schémas semblent
culturels, d'autres ont une allure plus fondamentale. Cela n'empêche
nullement l'unicité de l'individu, mais la société semble suivre des
lois probabilistes.

Mais si certaines idées semblent être transposables sans trop de perte
d'information d'une langue à l'autre, d'autres en ressortent
complètement déformées, quand elles ne débouchent pas sur une phrase qui
n'a plus de sens. Les idées représentées par des métaphores différentes
dans les langues impliquées sont particulièrement visées par ce
phénomène.

Et nous n'avons pas encore parlé de la sonorité. Si j'en juge par
l'échange animé, nous avons chacun une réaction différente par rapport à
la musique interne d'une langue. Je dirais pour résumer que, si un
concept apparemment simple comme l'amour à le même sens global en
Français et en Anglais, la phrase « je t'aime » suggère en complément
d'autres pensées que « I love you ».

Pour utiliser une métaphore musicale (oui, encore les ondes), je dirais
que le sens fondamental est le même dans les deux langues, mais pas les
harmoniques. En clair, la mélodie est la même, mais pas l'instrument. Hé
oui, on en revient au timbre.

Le cas des trente mots signifiant neige est plus complexe. Je suppose
que certains d'entre-eux décrivent des variantes de congère, de
poudreuse, etc. Traduire dans une langue un concept qui n'existe pas
sous forme de mot demande de contourner le problème en formant une
phrase, autrement dit en projetant la pensée d'une langue vers une
autre. Et qui dit projection, dit approximation, parfois grossière. Même
le sens fondamental peut alors varier.


