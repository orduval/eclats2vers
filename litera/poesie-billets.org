
#+STARTUP: showall

#+TITLE: Eclats de vers : Poésie : Billets
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index littéraire]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Corne d'abondance

** I

#+BEGIN_CENTER
#+BEGIN_VERSE
    Me méprendre ? Jamais ! Car toujours je suppose,
    Timide, direz-vous ? Mais le mot contient « Ose ! »
    Aussi lorsque je vois à deux pas de mes pattes
    Tomber le gant brodé d’une divine chatte,
    Je verse quelques vers dans sa coupe de cuivre,
    Mon penchant naturel m’incitant à la suivre
    Des gouttières de plomb aux jardins de Versailles.
    Qu’importe le buisson pourvu que le vers saille ?
    Que voulez-vous les chats qui rôdent sur les toits
    Choisissent le versant qui sied mieux à leur choix.
    Aussi, lorsque j’entends quelques strophes félines
    Se couler doucement derrière les courtines,
    Mon encrier déborde et je ne peux qu'écrire
    Jusqu'à ce que la nuit cède dans un soupir.
    Aussi je me permets, sur votre joue fugace,
    De laisser un baiser tout frémissant d’audace.
#+END_VERSE
#+END_CENTER

** II

#+BEGIN_CENTER
#+BEGIN_VERSE
    C’est ma foi fort bien dit, j’ai cru voir une danse
    Tant votre écrit fleuri regorge d'élégance !
    J’ai cru voir Cyrano et des liens dangereux
    Renaître du passé en anges bienheureux !

    Vous n'êtes pas persane, et je ne suis « tapis »
    Que dans l’ombre, espérant que la lune sourie.
    Je la raccroche donc, cette lune étoiléé,
    En attendant l'éclair d’un soleil printanier.

    Mais puisque vous osez me confier vos frissons,
    La rigueur hivernale enflammant les tisons,
    Je dirai sans frayeur mes secrets appétits ...
    Un autre jour peut-être, ou peut-être une nuit.

    Oui, vous avez raison, j’accepte l’amitié,
    Que puissent s’embrasser nos esprits déliés
    Sur nos plumes ravies d'échanger leurs délires
    En vers et poésie. Quant à votre soupir,

    Je le laisserai fondre avec la gourmandise
    Qui sied aux doux baisers des félines marquises
    Et garde le parfum de ce délice exquis
    En espérant qu’au chaud il fera des petits !
#+END_VERSE
#+END_CENTER

** III

#+BEGIN_CENTER
#+BEGIN_VERSE
    Vous me connaissez mal, oui c’est mal me connaître
    Que me penser pouvoir briser cette fenêtre
    Traverser cinq cent lieues d’un vol de goéland
    Les ailes en sueur et le plumage en fièvre
    Espérant le délit que me tendent vos lèvres
    Pour m’arrêter tout net le mors sur mes élans

    Car ne pas tout tenter serait vous faire injure
    Quant à votre refus il me serait torture
    Je vous l’ai déjà dit l'âme appelle la chair
    Dites si vous voulez ma morale bancale
    Mais mon sang latin nie les rigueurs boréales
    Qui font taire leur corps dans une ère glaciaire

    Car l’instinct du chasseur qui se réveille en moi
    N’attendrait qu’un soupir pour fondre sur sa proie
    Qu’arriverait-il si vous vous laissiez saisir ?
    Nos peaux seraient marquées que ce soit par l’absence
    Le remords la souffrance ou bien l’indifférence
    Ne tentez plus ce fou qui voudrait vous offrir

    Des coussins de tendresse et des barils d’amour
    Mais qui ne peut n’ayant la magie pour secours
    Se trouver et partout et toujours à la fois
    Car je vous veux heureuse et vous ferais souffrir
    Rengainons nos ardeurs je veux vous voir sourire
    Sur ce petit bisou juste au bord de vos draps
#+END_VERSE
#+END_CENTER

[[../index.php][Accueil]]
