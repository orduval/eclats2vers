
#+STARTUP: showall

#+TITLE: Eclats de vers : Agora 04 : Enseignement
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Principes

  - Plus de temps pour les parents
  - Enseigner le plus important
    + Logique
      * Éviter la confusion
      * Comment contrer les techniques de manipulation
    + Mathématiques
    + Droit, constitution
    + Finance
    + Art

* Généralités

** Cours de démocratie active

Expliquer aux jeunes qu’ils sont responsables de l'évolution
de la nation, que ce sont eux qui décident et que s’ils abdiquent,
la démocratie sera en danger.

Esprit démocratique : ouverture d’esprit, analyse, éviter
d’accepter ou de rejeter toute une théorie en bloc sans en
méditer chaque point, recherche d’un consensus et d’idées
originales qui pourraient satisfaire le plus grand nombre.


** Cours de droit

Présentation de la constitution. Description du système législatif
et des lois les plus importantes.

** Finance


** Économie
