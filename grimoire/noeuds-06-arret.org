
#+STARTUP: showall

#+TITLE: Eclats de vers : Noeuds 06 : Noeuds d’arrêt
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

#+TAGS: noexport(n)
#+TAGS: derived(d)
#+TAGS: middle_rope(m)
#+TAGS: sliding(s) adjustable_grip(g)
#+TAGS: double_loop(2) triple_loop(3)
#+TAGS: quick_release(q)
#+TAGS: vibration_proof(v)

* Introduction

Les noeuds d’arrêt sont utilisés pour empêcher une corde de passer à
travers une ouverture. Exemples :

  - empêcher un autre noeud de se défaire
  - empêcher une partie de corde servant de réglage de sortir de la
    boucle de serrage d’une boucle ajustable

* Figure 8 stopper knot

  1. faire un figure 8
  2. serrer

* Multiple figure 8 knot
:PROPERTIES:
:CUSTOM_ID: heading:multiple_figure_height
:END:

** Méthode de la boucle

Une boucle puis une corde qui joue à saute-mouton au milieu.

  1. faire une boucle *B*, au-dessus vers la droite
  2. répéter plusieurs fois, en progressant vers la voûte de *B*
     1) passer sous le bord droit de *B*, vers la gauche
     2) passer au-dessus du bord gauche de *B*, vers la gauche
     3) passer sous le bord gauche de *B*, vers la droite
     4) passer au-dessus du bord droit de *B*, vers la droite
  3. serrer en tirant sur la partie statique

** Méthode des boucles

Plusieurs boucles avec une corde qui joue à saute-mouton au milieu.

  1. faire une boucle *A*, au-dessus vers la droite
  2. passer sous la partie statique, vers la gauche
  3. faire un demi-cercle *B* anti-horlogique et passer au-dessus de
     la partie statique, vers la droite, juste devant le croisement de
     *A*
  4. passer sous la partie statique, vers la gauche
  5. faire un demi-cercle *C* anti-horlogique et passer au-dessus de
     la partie statique, vers la droite, juste devant le croisement de
     *B*
  6. passer sous la partie statique, vers la gauche
  7. vérifier que la boucle et les demi-cercles sont disposées en
     *C* - *B* - *A*, d’avant en arrière
  8. passer dans *C* en entrant par au-dessus et en sortant par
     en-dessous
  9. passer dans *B* en entrant par au-dessus et en sortant par
     en-dessous
  10. passer dans *A* en entrant par au-dessus et en sortant par
      en-dessous
  11. serrer, en ajustant le noeud pour qu’il prenne la forme de trois
      huits superposés

/Remarque/ : on peut généraliser le processus à un nombre quelconque
de boucles.

* Ashley stopper knot
:PROPERTIES:
:CUSTOM_ID: heading:ashley_stopper_knot
:END:

/alias/ : oysterman’s stopper knot

** Méthode du slip knot

Rentrer le terminus de la corde dans un slip knot de serrage.

  1. faire un slip knot, partie statique du côté réglable
  2. appelons *A* l’anse du slip knot
  3. orienter *A* pour que sa voûte soit vers le haut
  4. appelons *C* la boucle ceinturant la base de *A*
  5. tourner autour de *C*, en suivant le même sens de rotation que
     celui que la corde effectue dans *C*, jusqu’à ce que
     1) on arrive face à *A*
     2) on soit devant la partie de *C* qui ne contient qu’une seule
        partie de corde
  6. passer dans *A*
  7. tirer sur la partie de travail pour qu’elle passe entièrement
     dans *A*
  8. serrer *A* en tirant sur la partie statique

Si la partie de travail est passée dans le bon sens dans l’anse du
slip knot, la forme du noeud devrait compter quatre lobes.

** Méthode du bretzel

  1. faire un bretzel gauche inférieur
  2. choisir la partie libre de droite comme partie de travail
  3. faire un demi-tour horlogique
  4. passer sous la partie statique, vers la gauche
  5. aller jusqu’à la gauche du bretzel
  6. passer dans l’oreille gauche du bretzel, en entrant par
     en-dessous, vers la droite
  7. passer dans le triangle du bretzel en entrant par au-dessus, vers
     la droite
  8. serrer

* Sink stopper knot

** Standard

/alias/ : noeud d’arrêt canadien

Comme le Ashley stopper knot simple, mais faire un tour autour de la
base du slip knot avant de passer dans la boucle (l’anse) de serrage.

  1. faire un Slip knot, sans serrer, partie statique du côté réglable
  2. appelons *A* l’anse du Slip knot
  3. orienter *A* pour que sa voûte soit vers le haut
  4. appelons *C* la boucle ceinturant la base de *A*
  5. faire un tour et demi autour de *A* en suivant *C*
  6. passer dans *A*
  7. tirer sur la partie de travail pour qu’elle passe entièrement
     dans *A*
  8. serrer *A* en tirant sur la partie statique

** Variante à plusieurs tours

Comme le Ashley stopper knot simple, mais faire plusieurs tours autour
de la base du slip knot avant de passer dans la boucle (l’anse) de
serrage.

* Heaving line knot
:PROPERTIES:
:CUSTOM_ID: heading:heaving_line_knot
:END:

/alias/ : franciscan monks knot

** Simple

Ressemble fort à une [[file:noeuds-03-base.org::#heading:boucle_a_bobine][boucle à bobine]] autonome.

  1. faire une boucle *B*, au-dessus vers la droite
  2. passer dans *B* en entrant sous le bord droit
     - juste derrière le croisement de *B*
  3. passer au-dessus du bord droit de *B*
  4. continuer à touner dans le même sens pour réaliser une bobine
     autour des bords de *B*, en progressant vers la voûte
     - laisser une petite partie de *B* visible
       + soit *V* cette partie visible
  5. passer dans *B*
  6. serrer en tirant sur la partie statique

** Variante en esse

  1. faire un esse à droite, vers l’arrière
     - soit *H* l’anse de gauche du esse
     - soit *A* l’anse de droite du esse
  2. passer dans *H*
     - en entrant sous le bord droit
     - vers la gauche
     - près de la voûte
  3. passer au-dessus du bord gauche de *H*, vers la gauche
  4. tour autour de *H*
     1) passer sous les bords de *H*
        - vers la droite
        - un peu plus en avant
     2) passer au-dessus des bords de *H*, vers la gauche
  5. faire une bobine *B* autour de *H* et de *D*
     1) passer sous les bords de *H* et *D*
        - vers la droite
        - un peu plus en avant
     2) passer au-dessus des bords de *H* et *D*, vers la gauche
     3) continuer à enrouler la corde autour de *H* et *D*
        - en suivant le même sens de rotation
        - en progressant vers la voûte de *A*
        - laisser une petite partie de *A* visible, près de la voûte
          + soit *V* cette partie visible
  6. passer dans *V*
  7. serrer *V* autour de la partie de travail

* Doughnut heaving line knot
:PROPERTIES:
:CUSTOM_ID: heading:doughnut_heaving_line
:END:

NOeud d’arrêt en forme de tore : bobine auxiliaire enroulée autour
d’une bobine principale. Nécessite une grande longueur de corde.

  1. orienter la partie statique vers l’avant
  2. faire une bobine principale *B* de quelques tours
    - assez petite
    - en tournant dans le sens horlogique
    - en progressant vers le haut
    - commencer et terminer à l’arrière
  3. soit *R* le bord arrière de *B*
  4. effectuer une boucle *S* autour de *R*
    - à droite de la partie statique
  5. réaliser une bobine auxiliaire qui fait le tour des bords de *B*
    - en progressant dans un sens anti-horlogique
  6. serrage
     1) passer entre *B* et *S*
     2) tirer sur la partie adjacente de *B* pour serrer *S* autour de
        la partie de travail

* Stevedore knot
:PROPERTIES:
:CUSTOM_ID: heading:stevedore_knot
:END:

Boucle à bobine autonome, à rebours, externe.

  1. faire une boucle
  2. faire une bobine de quelques tours autour de la partie statique,
     en s’éloignant de la boucle
     - la bobine est souvent de deux tours
  3. passer dans la boucle
  4. serrer la boucle autour de la partie de travail

* Noeud en trois lobes

J’ai découvert ce noeud en tentant de réaliser trois boucles dans des
plans perpendiculaires. Si il est déjà catalogué et que vous en
connaissez le nom, merci de me le signaler. Il ressemble à l’Ashley
stopper knot, mais la structure du noeud est légèrement différente.

  1. faire un brezn supérieur droit
  2. faire un demi-tour horlogique
  3. passer sous le brezn, vers l’arrière gauche
  4. entrer par en-dessous dans l’oreille gauche du brezn, vers
     l’arrière gauche
  5. serrer

* Celtic button

  1. faire une boucle *A*, au-dessus vers la droite
  2. faire une boucle *B* à droite de *A*, partie de travail au-dessus
     vers la droite
  3. passer la partie droite de *A* sous la partie gauche de *B*
  4. faire un demi-cercle anti-horlogique *C*, vers la gauche, à
     droite de *B*
  5. passages alternés dans les régions
     1) passer au-dessus du bord droit de *B*
     2) passer sous le bord droit de *A*
     3) passer au-dessus du bord gauche de *B*
     4) passer sous le bord gauche de *A*
  6. passer dans *C* en entrant par au-dessus, vers la gauche
  7. passer sous le bord droit de *B*, vers la gauche
  8. passer sous le bord droit de *A*, vers la gauche
  9. sortir de *I* par au-dessus, vers l’arrière
  10. serrer

* Monkey’s fist

/alias/ : pomme de touline

  1. faire un noeud qui prend de la place
     - servira de noyau
     - alternative : utiliser un petit objet arrondi comme noyau
  2. enrouler une première bobine *A* autour du noyau
  3. enrouler une deuxième bobine *B*
     - autour d’un axe perpendiculaire à l’axe de *A*
     - autour de *A*
     - ne pas enrouler sur toute la longueur de *A* mais seulement au
       milieu, laisser des espaces sur les côtés pour la troisième
       bobine
  4. enrouler une troisième bobine *C*
     - autour d’un axe perpendiculaire aux axes de *A* et *B*
     - autour de *B*
     - en passant dans les espaces laissés ouverts à l’intérieur de *A*
  5. serrer
