; -*- lisp -*-
; vim : set filetype=lisp :

(add-to-list 'load-path "~/racine/plugin/manager/el-get/htmlize")
(add-to-list 'load-path "~/racine/plugin/manager/el-get/org-mode")

(require 'htmlize)
(require 'org)
