
#+STARTUP: showall

#+TITLE: Eclats de vers : Danse 01 : Quadrille
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Introduction

La danse est une géométrie dynamique en trois dimensions.


* Danses traditionnelles

  - ronde
  - farandole
  - quadrille
  - gigue
  - menuet
