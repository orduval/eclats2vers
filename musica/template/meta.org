
#+STARTUP: showall

#+TITLE: Eclats de vers : Opus : Musica : Template : Méta
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+INCLUDE: "../../include/navigan-1.org"

#+TOC: headlines 1

#+TAGS: noexport(n)

* Méta-orchestre

Le résultat :

[[file:../../image/musica/template/meta.png]]

Ce fichier peut être utilisé pour construire l’orchestre que vous
souhaitez, en décommentant ou commentant les instruments dans la section
"book" à la fin du fichier source, que voici :

#+include: "~/racine/musica/lilypond/template/meta.ly" src lilypond
