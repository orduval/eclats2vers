
#+STARTUP: showall

#+TITLE: Eclats de vers : Matemat 01 : Ensembles - 4
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

#+INCLUDE: "../include/commandes-tex.org"

* Fonctions et ordre

#+TOC: headlines 1 local

\label{chap:fonctionsEtOrdre}


** Dépendances

  - Chapitre \ref{chap:ordres} : Les ordres
  - Chapitre \ref{chap:extrema} : Les extrema


** Monotonie


*** Croissance

On dit qu'une fonction $f : A \to F$ est croissante si :

$$f(x) \ge f(y)$$

pour tout $x,y \in A$ tels que $x \ge y$. Autrement dit,
une fonction croissante conserve l'ordre.


*** Décroissance

On dit qu'une fonction $f : A \to F$ est décroissante si :

$$f(x) \le f(y)$$

pour tout $x,y \in A$ tels que $x \ge y$. Autrement dit,
une fonction décroissante inverse l'ordre.


*** Stricte

On dit qu'une fonction $f : A \to F$ est strictement croissante si :

$$f(x) \strictinferieur f(y)$$

pour tout $x,y \in A$ tels que $x \strictinferieur y$.

On dit qu'une fonction $f : A \to F$ est strictement décroissante si :

$$f(x) \strictsuperieur f(y)$$

pour tout $x,y \in A$ tels que $x \strictinferieur y$.


** Ordre entre fonctions

Soit les fonctions $f,g \in B^A$. Supposons qu'il existe un ordre défini
sur $B$. On dit que $f$ est inférieure à $g$ et on le note :

$$f \le g$$

si et seulement si les valeurs de $f$ sont inférieures aux valeurs de $g$ :

$$f(x) \le g(x)$$

en tout point $x \in A$. Symétriquement, on dit que $f$ est supérieure à $g$ et on le note :

$$f \ge g$$

si :

$$f(x) \ge g(x)$$

pour tout $x \in A$.


*** Strict

On note également :

  - $f \strictinferieur g$ si $f(x) \strictinferieur g(x)$ pour tout $x \in A$
  - $f \strictsuperieur g$ si $f(x) \strictsuperieur g(x)$ pour tout $x \in A$


** Fonctions extrema

Soit un ensemble de paramètres $Z$ et la collection paramétrée de fonctions :

$$\{ f_z \in B^A : z \in Z \}$$

Sous réserve d'existence des extrema, on définit la fonction supremum par :

$$\left[ \sup_{z \in Z} f_z \right](x) = \sup_{z \in Z} f_z(x)$$

pour tout $x \in A$. On définit la fonction infimum par :

$$\left[ \inf_{z \in Z} f_z \right](x) = \inf_{z \in Z} f_z(x)$$

pour tout $x \in A$.


*** Maximum et minimum

Lorsque les maximum et minimum existent, on définit :

#+BEGIN_CENTER
\(
\left[ \max_{z \in Z} f_z \right](x) = \max_{z \in Z} f_z(x) \\ \\
\left[ \min_{z \in Z} f_z \right](x) = \min_{z \in Z} f_z(x)
\)
#+END_CENTER


*** Notation

On note aussi :

#+BEGIN_CENTER
\(
\sup \{ f_z : z \in Z \} = \sup_{z \in Z} f_z \\ \\
\inf \{ f_z : z \in Z \} = \inf_{z \in Z} f_z \\ \\
\max \{ f_z : z \in Z \} = \max_{z \in Z} f_z \\ \\
\min \{ f_z : z \in Z \} = \min_{z \in Z} f_z
\)
#+END_CENTER


*** Couples

On définit les fonctions $M = \max\{f,g\}$ et $m = \min\{f,g\}$ par :

#+BEGIN_CENTER
\(
M(x) = \max\{ f , g \}(x) = \max\{ f(x) , g(x) \} \\
m(x) = \min\{ f , g \}(x) = \min\{ f(x) , g(x) \}
\)
#+END_CENTER

pour tout $x \in A$.


** Ordre mixte

Soit la fonction $f \in B^A$ et un $c \in B$. Supposons qu'il existe un ordre défini sur $B$. On dit que $f$ est inférieure à $c$ et on le note :

$$f \le c$$

si les valeurs de $f$ sont inférieures à $c$ :

$$f(x) \le c$$

en tout point $x \in A$. Symétriquement, on dit que $f$ est supérieure à $c$ et on le note :

$$f \ge c$$

si :

$$f(x) \ge c$$

pour tout $x \in A$.


*** Strict

On note également :

  - $f \strictinferieur c$ si $f(x) \strictinferieur c$ pour tout $x \in A$.
  - $f \strictsuperieur c$ si $f(x) \strictsuperieur c$ pour tout $x \in A$.


*** Fonctions max et min

On définit les fonctions $\max\{f,c\}$ et $\min\{f,c\}$ par :

#+BEGIN_CENTER
\(
\max\{ f , c \}(x) = \max\{ f(x) , c \} \\
\min\{ f , c \}(x) = \min\{ f(x) , c \}
\)
#+END_CENTER

pour tout $x \in A$.


** Extrema d'une fonction

Etant donné la fonction $f : \Omega \mapsto B$ et le sous-ensemble $A \subseteq \Omega$, on définit les extrema de $f$ (s'ils existent) par :

#+BEGIN_CENTER
\(
\max_{x \in A} f(x) = \max \{ f(x) : x \in A \} \\ \\
\min_{x \in A} f(x) = \min \{ f(x) : x \in A \} \\ \\
\sup_{x \in A} f(x) = \sup \{ f(x) : x \in A \} \\ \\
\inf_{x \in A} f(x) = \inf \{ f(x) : x \in A \}
\)
#+END_CENTER


** Arguments d'extrema


*** Maximum et minimum

L'ensemble des éléments de $A$ qui maximisent $f$ sur $A$ est noté :

$$\arg\max_{x \in A} f(x) = \left\{ \alpha \in A : f(\alpha) = \max_{x \in A} f(x) \right\}$$

Dans le cas où cet ensemble contient un unique élément, on le note :

$$\alpha = \arg\max_{x \in A} f(x)$$

L'ensemble des éléments de $A$ qui minimisent $f$ sur $A$ est noté :

$$\arg\min_{x \in A} f(x) = \left\{ \beta \in A : f(\beta) = \min_{x \in A} f(x) \right\}$$

Dans le cas où cet ensemble contient un unique élément, on le note :

$$\beta = \arg\min_{x \in A} f(x)$$


*** Supremum et infimum

L'ensemble des éléments de $\Omega$ qui produisent une valeur égale au supremum des valeurs de $f$ sur $A \subseteq \Omega$ est noté :

$$\argument_\Omega\sup_{x \in A} f(x) = \left\{ \alpha \in \Omega : f(\alpha) = \sup_{x \in A} f(x) \right\}$$

Dans le cas où cet ensemble contient un unique élément, on le note :

$$\alpha = \argument_\Omega\sup_{x \in A} f(x)$$

L'ensemble des éléments de $\Omega$ qui produisent une valeur égale à l'infimum des valeurs de $f$ sur $A \subseteq \Omega$ est noté :

$$\argument_\Omega\inf_{x \in A} f(x) = \left\{ \beta \in \Omega : f(\beta) = \inf_{x \in A} f(x) \right\}$$

Dans le cas où cet ensemble contient un unique élément, on le note :

$$\beta = \argument_\Omega\inf_{x \in A} f(x)$$


** Extrema locaux

On dit que $f$ atteint un minimum local en $a \in A$ si il existe un voisinage $U$ de $x$ tel que :

$$f(a) \le f(x)$$

pour tout $x \in U$. A l'inverse, on dit que $f$ atteint un maximum local en $a \in A$ si il existe un voisinage $U$ de $x$ tel que :

$$f(a) \ge f(x)$$

pour tout $x \in U$.


** Ordre entre fonctions et extrema

Soit les fonctions $f,g : A \mapsto B$ telles que $f \le g$. Supposons que $\sigma \in \major g(A)$. On a $\sigma \ge g(x) \ge f(x)$ pour tout $x \in A$, d'où $\sigma \ge f(x)$ et $\sigma \in \major f(A)$. On en conclut que $\major g(A) \subseteq \major f(A)$. Si les minima existent, on a donc :

$$\min \major f(A) \le \min \major g(A)$$

c'est-à-dire :

$$\sup_{x \in A} f(x) = \sup f(A) \le \sup g(A) = \sup_{x \in A} g(x)$$

Supposons que $\lambda \in \minor f(A)$. On a $\lambda \le f(x) \le g(x)$ pour tout $x \in A$, d'où $\lambda \le g(x)$ et $\lambda \in \minor g(A)$. On en conclut que $\minor f(A) \subseteq \minor g(A)$. Si les maxima existent, on a donc :

$$\max \minor f(A) \le \max \minor g(A)$$

c'est-à-dire :

$$\inf_{x \in A} f(x) = \inf f(A) \le \inf g(A) = \inf_{x \in A} g(x)$$


* Bijections

#+TOC: headlines 1 local

\label{chap:bijections}


** Dépendances

  - Chapitre \ref{chap:fonctionsEtOrdre} : Fonctions et ordre


** Inverse à gauche

Soit $f : A \mapsto B$. Si $l : B \mapsto A$ est une fonction telle que :

$$l \circ f = \identite$$

on dit que $l$ est un inverse à gauche de $f$.


*** Unicité

Soit $y \in B$. Si $f$ admet un inverse à gauche, on peut trouver au plus un seul $x \in A$ tel que $f(x) = y$. En effet, supposons que $x_1, x_2 \in A$ avec $f(x_1) = f(x_2) = y$. On a :

$$l(y) = (l \circ f)(x_1) = (l \circ f)(x_2)$$

mais comme $l \circ f = \identite$, et que $\identite(x) = x$ pour tout $x \in A$, on a :

$$l(y) = x_1 = x_2$$

ce qui prouve l'unicité de la solution.


** Inverse à droite

Soit $f : A \mapsto B$. Si $r : B \mapsto A$ est une fonction telle que :

$$f \circ r = \identite$$

on dit que $r$ est un inverse à droite de $f$.


*** Existence

Soit $y \in B$. Si $f$ admet un inverse à droite $r$, on peut trouver au moins un $x \in A$ tel que $f(x) = y$. En effet, si l'on choisit $x = r(y)$, on a :

$$f(x) = (f \circ r)(y) = \identite(y) = y$$

ce qui prouve l'existence de la solution.


** Fonction inverse

Soit $f : A \mapsto B$. Supposons que $f$ admette à la fois un inverse à gauche $l$ et un inverse à droite $r$. Ces deux inverses sont dès lors identiques :

$$l = l \circ \identite = l \circ f \circ r = \identite \circ r = r$$


*** Existence et unicité

Soit $y \in B$. On déduit des résultats précédents que l'on peut trouver un unique $x \in A$, donné par $x = r(y)$, tel que $f(x) = y$. Nous pouvons dès lors définir la fonction inverse, notée $f^{-1} : B \mapsto A$, par :

$$f^{-1}(y) = r(y)$$

pour tout $y \in B$. On a donc :

$$f^{-1} = r = l$$

et :

$$f^{-1} \circ f = f \circ f^{-1} = \identite$$


*** Bijection

Lorsque $f$ admet un inverse $f^{-1}$, on dit que $f$ est inversible ou que $f$ est une bijection. On note :

$$\bijection(A,B) = \{ f \in B^A : \exists f^{-1} \in A^B \}$$

l'ensemble des bijections de $A$ vers $B$.


** Relation

Lorsque $f$ est inversible, l'image inverse de $y \in B$ se réduit à un ensemble contenant un unique $x \in A$. Soit $R$ la relation associée à $f$. On a :

$$R^{-1}(y) = \{ f^{-1}(y) \}$$


** Inverse d'une composée

Soit $f \in \bijection(A,B)$ et $g \in \bijection(B,C)$. Nous allons essayer de trouver une expression de l'inverse $h$ de la composée de ces deux fonctions. On part de la relation $h \circ g \circ f = \identite$ et on compose à droite par l'inverse de $f$ puis l'inverse de $g$, ce qui nous donne :

$$h \circ g \circ f \circ f^{-1} \circ g^{-1} = \identite \circ f^{-1} \circ g^{-1} = f^{-1} \circ g^{-1}$$

Mais comme :

$$h \circ g \circ f \circ f^{-1} \circ g^{-1} = h \circ g \circ \identite \circ g^{-1} = h \circ g \circ g^{-1} = h \circ \identite = h$$

on a :

$$h = f^{-1} \circ g^{-1}$$

Partant de la relation $g \circ f \circ h = \identite$ et composant à gauche par l'inverse de $g$ puis par l'inverse de $f$, on arrive au même résultat $h = f^{-1} \circ g^{-1}$. Donc :

$$(g \circ f)^{-1} = f^{-1} \circ g^{-1}$$

On généralise par récurrence à $n$ fonctions :

$$(f_n \circ ... \circ f_2 \circ f_1)^{-1} = f_1^{-1} \circ f_2^{-1} \circ ... \circ f_n^{-1}$$


** Puissances négatives

Si $f$ est inversible, on peut également définir les puissances négatives par :

$$f^{-n} = \left( f^{-1} \right)^n = f^{-1} \circ ... \circ f^{-1}$$

pour tout $n \in \setN$.


** Inverse d'une puissance

En considérant le cas particulier de fonctions identiques ($f_1 = ... = f_n = f$), l'expression de l'inverse d'une composition de fonctions nous montre que :

$$\left( f^{-1} \right)^n = f^{-n} = \left( f^n \right)^{-1}$$

L'inverse de la puissance est identique à la puissance de l'inverse.


** Idempotence

On dit que deux ensembles $A$ et $B$ sont idempotents si il existe une bijection inversible de $A$ vers $B$ :

$$\bijection(A,B) \ne \emptyset$$


*** Equivalence

Soit les ensembles $A$ et $B$. Supposons qu'il existe une bijection $f \in \bijection(A,B)$. On peut alors définir une équivalence sur $A \cup B$ en disant que $x \equiv y$ si $x \in A$ avec $y = f(x) \in B$, ou si $x \in B$ avec $y = f^{-1}(x) \in A$. Dans les autres cas, $x$ ne sera pas équivalent à $y$. Les classes d'quivalences seront donc de la forme :

$$\mathcal{E}(a) = \{ a , f(a) \}$$

pour tout $a \in A$ et de la forme :

$$\mathcal{E}(b) = \{ b , f^{-1}(b) \}$$

pour tout $b \in B$. Il y a donc une « équivalence » entre les éléments de $A$ et les éléments de $B$. Dans ce cas, on confond souvent les éléments $x$ de $A$ avec les éléments de $\hat{x}$ de $B$, et on note abusivement $x = \hat{x}$.


*** Ensembles finis

Dans le cas d'ensembles finis, l'idempotence revient à dire que $A$ et $B$ ont le même nombre d'éléments.


** Inverse des fonctions monotones

\begin{theoreme}

Soit la fonction $f : A \mapsto B$ avec $B = f(A)$. Si $f$ est strictement croissante (ou décroissante), alors la fonction $f$ est inversible.

\end{theoreme}

\begin{demonstration}

Choisissons $y \in B$ et considérons l'ensemble de solutions :

$$S(y) = \{ x : f(x) = y \}$$

Comme $B = f(A)$, cet ensemble est non vide. Supposons $x_1,x_2 \in S(y)$ avec $x_1 \strictinferieur x_2$. On a alors, soit $f(x_1) \strictinferieur f(x_2)$ (si $f$ est strictement croissante), soit $f(x_1) \strictsuperieur f(x_2)$ (si $f$ est strictement décroissante). Or, par définition de $S(y)$, on doit avoir $f(x_1) = f(x_2) = y$. Il ne peut donc y avoir d'éléments distincts dans $S(y)$, et cet ensemble est de la forme :

$$S(y) = \{ g(y) \}$$

relation qui définit implicitement la fonction $g : B \mapsto A$. On peut trouver un unique $g(y)$ tel que $f(g(y)) = y$. La fonction $f$ est donc inversible et :

$$f^{-1} = g$$

\end{demonstration}


* Ordre inclusif

#+TOC: headlines 1 local

\label{chap:ordreInclusif}


** Dépendances

  - Chapitre \ref{chap:collections} : Les collections
  - Chapitre \ref{chap:ordres} : Les ordres
  - Chapitre \ref{chap:extrema} : Les extrema


** Ordre sur les ensembles

Soit l'ensemble $\Omega$ et l'ensemble de ses sous-ensembles :

$$\sousens(\Omega) = \{ A : A \subseteq \Omega \}$$

On dit que $A \in \sousens(\Omega)$ est plus petit ou égal à $B \in \sousens(\Omega)$ au sens de l'ordre inclusif si et seulement si :

$$A \subseteq B$$

On voit qu'il s'agit d'un ordre partiel.


*** Attention

Ne pas confondre l'ordre inclusif $\subseteq$ avec la comparaison d'ensembles $\ensinferieur$ qui est elle basée sur l'ordre entre les éléments des ensembles concernés.


** Extrema inclusifs d'une collection

Soit l'ensemble de paramètres $X$ et la collection paramétrée :

$$\mathcal{C} = \{ A(x) \in \sousens(\Omega) : x \in X \}$$


*** Majorant

Si on veut qu'un ensemble quelconque $M \subseteq \Omega$ soit un majorant inclusif de $\mathcal{C}$, il faut que $A(x) \subseteq M$ pour tout $x \in X$. Pour cela, il faut et il suffit que $M$ contienne tous les $A(x)$. Autrement dit, il faut et il suffit que $M$ contienne l'union des ensembles-éléments de la collection. On a donc :

$$\major_\subseteq \mathcal{C} = \left\{ M \in \sousens(\Omega) : \bigcup_{x \in X} A(x) \subseteq M \right\}$$


*** Supremum

Etudions l'ensemble :

$$S = \bigcup_{x \in X} A(x) \in \sousens(\Omega)$$

Comme :

$$\bigcup_{x \in X} A(x) \subseteq S$$

on a :

$$S \in \major_\subseteq \mathcal{C}$$

On voit aussi que $S \subseteq M$ pour tout $M \in \major_\subseteq \mathcal{C}$. On en déduit que :

$$S = \min_\subseteq \major_\subseteq \mathcal{C} = \sup_\subseteq \mathcal{C}$$


*** Minorant

Si on veut qu'un ensemble quelconque $L \subseteq \Omega$ soit un minorant inclusif de $\mathcal{C}$, il faut que $L \subseteq A(x)$ pour tout $x \in X$. Pour cela, il faut et il suffit que $L$ soit contenu dans tous les $A(x)$. Autrement dit, il faut et il suffit que $L$ soit contenu dans l'intersection des ensembles-éléments de la collection. On a donc :

$$\minor_\subseteq \mathcal{C} = \left\{ L \in \sousens(\Omega) : L \subseteq \bigcap_{x \in X} A(x) \right\}$$


*** Infimum

Etudions l'ensemble :

$$I = \bigcap_{x \in X} A(x) \in \sousens(\Omega)$$

Comme :

$$I \subseteq \bigcap_{x \in X} A(x)$$

on a :

$$I \in \minor_\subseteq \mathcal{C}$$

On voit aussi que $L \subseteq I$ pour tout $L \in \minor_\subseteq \mathcal{C}$. On en déduit que :

$$I = \max_\subseteq \minor_\subseteq \mathcal{C} = \inf_\subseteq \mathcal{C}$$


*** Conclusion

Le supremum inclusif d'une collection de sous-ensembles de $\Omega$ existe toujours dans $\sousens(\Omega)$ et est égal à l'union de tous ces ensembles :

$$\sup_\subseteq \{ A(x) \in \sousens(\Omega) : x \in X \} = \bigcup_{x \in X} A(x)$$

L'infimum inclusif d'une collection de sous-ensembles de $\Omega$ existe toujours dans $\sousens(\Omega)$ et est égal à l'intersection de tous ces ensembles :

$$\inf_\subseteq \{ A(x) \in \sousens(\Omega) : x \in X \} = \bigcap_{x \in X} A(x)$$
