
#+STARTUP: showall

#+TITLE: Eclats de vers : Pictura 01 : Géométrie
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

#+INCLUDE: "../include/commandes-tex.org"

* Base

#+TOC: headlines 1 local


** Angle droit

Considérons deux droites perpendiculaires. Elles forment au croisement
quatre angles $\alpha$ identiques qui forment un tour complet. On a
donc :

$$4 \alpha = 360°$$

d’où :

$$\alpha = 90 °$$


** Angles opposés d’une intersection

Soit deux droites qui s’intersectent. On note

$$\alpha, \beta, \gamma, \delta$$

les angles formés par cette intersection, dans le sens inverse des
aiguilles d’une montre.

On note que deux de ces angles consécutifs forment un demi-tour :

$$\alpha + \beta = 180 ° \\
\beta + \gamma = 180 ° \\
\gamma + \delta = 180 ° \\
\delta + \alpha = 180 °$$

On en déduit que :

$$\alpha = \gamma \\
\beta = \delta$$

Les angles opposés d’une intersection sont égaux.


* Somme des angles


** Virage

Voir cette vidéo de Mikaêl Launay, qui offre une méthode élégante pour
obtenir les sommes des angles d’un polygone :

https://www.youtube.com/watch?v=7wY-Byh8d6M

L’angle représente la rotation autour du sommet que doit effectuer la
première des demi-droites qui le définissent pour arriver jusqu’à la
seconde demi-droite.

Le virage représente la rotation que doit effectuer un objet qui
arrive par la première demi-droite pour suivre la seconde demi-droite.

Si l’angle $α$ et le virage $λ$ sont définis par les mêmes
demi-droites, on a :

$$λ = 180° - α$$

Considérons un polygone de $N$ côtés non étoilé, ce qui signifie que
les sommets sont les seules intersections entre les côtés.

Un objet parcourant le périmètre de ce polygone va tourner à chaque
sommet, toujours dans la même direction, jusqu’à revenir au point de
départ. La somme des virages correspondant aux angles internes du
polygone vaut donc 360°. Nous en déduisons que :

$$360° = \sum \mathrm{virages} = \sum (180° - \mathrm{angle}) = N × 180° - \sum \mathrm{angles}$$

donc :

$$\sum \mathrm{angles} = (N - 2) \times 180°$$

** Somme des angles d’un triangle

En appliquant la formule générale au cas $N = 3$, nous obtenons :

$$\sum \mathrm{angles} = 180°$$

** Somme des angles d’un quadrilatère

En appliquant la formule générale au cas $N = 4$, nous obtenons :

$$\sum \mathrm{angles} = 360°$$


** Somme des angles d’un pentagone

En appliquant la formule générale au cas $N = 5$, nous obtenons :

$$\sum \mathrm{angles} = 540°$$


** Somme des angles d’un hexagone

En appliquant la formule générale au cas $N = 6$, nous obtenons :

$$\sum \mathrm{angles} = 720°$$


* Parallèle & symétrie


** Cas particulier




* Cercle


** Demi-angle & cercle




*** Cas particulier 1




*** Cas particulier 2




** Triangle rectangle & cercle




** Hexagone et triangles équilatéraux




* Surface du triangle


** Surface d’un triangle rectangle




** Surface d’un triangle acutangle




** Surface d’un triangle obtusangle




* Pythagore


** Théorème de Pythagore

Surface [1] :

\begin{align}
(a + b)^2 &= \left( \frac{a \cdot b}{2} \right) \cdot 4  + c^2 \\
&= 2 a b + c^2
\end{align}

Surface [2] :

\begin{align}
(a + b)^2 &= a^2 + b^2 + 4 \cdot \left( \frac{a \cdot b}{2}  \right) \\
&= a^2 + b^2 + 2 a b
\end{align}

En comparant [1] & [2], on obtient :

$$2 a b + c^2 = a^2 + b^2 + 2 a b$$

c’est-à-dire :

$$c^2 = a^2 + b^2$$



** Triangles de Pythagore

Les triangles de Pythagore sont des triangles rectangles où les
longueurs des côtés peuvent être exprimés comme des multiples entiers
d’une même unité de mesure.

Voici les proportions des premiers triangles de Pythagore :

  - 3 - 4 - 5
    + $3^2 + 4^2 = 9 + 16 = 25 = 5^2$
  - 5 - 12 - 13
  - 7 - 24 - 25
  - 8 - 15 - 17
  - 9 - 40 - 41
  - 48 - 55 - 73


* Nombre d’or

$$x^2 = x + 1$$

$$x^2 - x - 1 = 0$$

$$\Delta = 1 - 4 \cdot 1 \cdot (-1) = 5$$

$$x = \frac{1 \pm \sqrt{5}}{2}$$

$$\phi = \frac{1 + \sqrt{5}}{2}$$

$$\phi^2 = \phi + 1$$

$$\unsur{\phi} = \phi - 1$$


* Trigonométrie


** Angle complémentaire

$$\cos \alpha = sin \beta = \frac{a}{c}$$

$$\sin \alpha = \cos \beta = \frac{b}{c}$$

Somme des angles du triangle rectangle :

$$\alpha + \beta + 90° = 180°$$

$$\beta = 90° - \alpha$$

$$\cos \alpha = \sin(90° - \alpha)$$

$$\sin \alpha = \cos(90° - \beta)$$


** Trigonométrie et théorème de Pythagore

$$\cos \alpha = \frac{a}{c}$$

$$\sin \alpha = \frac{b}{c}$$

$$a = c \cos \alpha$$

$$b = c \sin \alpha$$

Théorème de Pythagore :

$$a^2 + b^2 = c^2$$

$$c^2 \left( \cos \alpha \right)^2 + c^2 \left( \sin \alpha \right)^2 = c^2$$

$$(\cos \alpha)^2 + (\sin \alpha)^2 = 1$$


** Somme des angles


*** Sin & Cos $\alpha + \beta$

$$\sin(\alpha + \beta) = \sin\alpha \cos\beta + \sin\beta \cos\alpha$$

$$\cos(\alpha + \beta) = \cos\alpha \cos\beta - \sin\alpha \sin\beta$$


*** Tan $\alpha + \beta$

$$\tan\alpha = \frac{\sin\alpha}{\cos\alpha}$$

$$\tan\beta = \frac{\sin\beta}{\cos\beta}$$

$$\tan(\alpha + \beta) = \frac{\sin(\alpha + \beta)}{\cos(\alpha + \beta)}$$

$$\tan(\alpha + \beta) =
\frac{\sin\alpha \cos\beta + \sin\beta \cos\alpha}{\cos\alpha \cos\beta - \sin\alpha \sin\beta}$$

En divisant numérateur et dénominateur par $\cos\alpha \cos\beta$ :

$$\tan(\alpha + \beta) =
\frac{\frac{\sin\alpha}{\sin\beta} +
\frac{\sin\beta}{\cos\beta}}{1 - \frac{\sin\alpha \sin\beta}{\cos\alpha \cos\beta}}$$

c’est-à-dire :

$$\tan(\alpha + \beta) = \frac{\tan\alpha + \tan\beta}{1 - \tan\alpha \tan\beta}


** Angle double

Formules des sommes $\alpha + \beta$ avec $\alpha = \beta$ :

$$\sin(2 \alpha) = 2 \sin\alpha \cos\alpha$$

$$\cos(2 \alpha) = (\cos\alpha)^2 - (\sin\alpha)^2$$

Comme $(\cos\alpha)^2 + (\sin\alpha)^2 = 1$ :

$$\cos(2 \alpha) = 1 - 2 (\sin\alpha)^2$$

$$\cos(2 \alpha) = 2 (\cos\alpha)^2 - 1$$

$$\tan(2 \alpha) = \frac{2 \tan\alpha}{1 - (\tan\alpha)^2}$$


** Angle moitié

En inversant les relations de $\cos(2 \alpha)$ :

$$(\sin\alpha)^2 = \frac{1 - \cos(2 \alpha)}{2}$$

$$(\cos\alpha)^2 = \frac{1 + \cos(2 \alpha)}{2}$$

c’est-à-dire :

$$\sin\alpha = \pm \sqrt{\frac{1 - \cos(2 \alpha)}{2}}$$

$$\cos\alpha = \pm \sqrt{\frac{1 + \cos(2 \alpha)}{2}}$$

Une conséquence immédiate :

$$\tan\alpha = \pm \sqrt{\frac{1 - \cos(2 \alpha)}{1 + \cos(2 \alpha)}}$$


** Sin & Cos des angles de base


*** Sin & Cos 45°

Nous considérons un triangle rectangle avec 2 côtés égaux.

Somme des angles du triangle :

$$90° + \alpha + \beta = 180°$$

Symétrie :

$$\alpha = \beta$$

On en déduit :

$$\alpha = \frac{180° - 90°}{2} = 45°$$

$$\alpha = \beta = 45°$$

Théorème de pythagore :

$$b^2 = a^2 + a^2 = 2 a^2$$

$$b = \sqrt{2} a$$

$$\cos 45° = \frac{a}{b}
= \frac{a}{\sqrt{2} a}
= \unsur{\sqrt{2}}
= \frac{\sqrt{2}}{2}$$

$$\sin 45° = \frac{a}{b} = \frac{\sqrt{2}}{2}$$

$$\cos 45° = \sin 45° = \frac{\sqrt{2}}{2}$$


*** Sin 30°, Cos 60°

$$\sin 30° = \cos 60° = \unsur{2}$$


*** Sin 60°, Cos 30°

Cosinus de 30° :

$$(\sin 30°)^2 + (\cos 30°)^2 = 1$$

$$\left(\unsur{2}\right)^2 + (\cos 30°)^2 = 1$$

$$(\cos 30°)^2 = 1 - \unsur{4} = \frac{3}{4}$$

$$\cos 30° = \frac{\sqrt{3}}{2}$$

Sinus de 60° :

$$(\sin 60°)^2 + (\cos 60°)^2 = 1$$

$$(\sin 60°)^2 + \left(\unsur{2}\right)^2 = 1$$

$$(\sin 60°)^2 = 1 - \unsur{4} = \frac{3}{4}$$

$$\sin 60° = \frac{\sqrt{3}}{2}$$


*** Cos 36°




*** Sin 36°




** Applications des formules de la tangente


*** Triangles 1/2 & 1/3

$$\tan\alpha = \unsur{2}$$

$$\tan\beta = \unsur{3}$$

$$\tan(\alpha + \beta) = \frac{\unsur{2} + \unsur{3}}{1 - \unsur{2} \unsur{3}} = \frac{5/6}{1 - 1/6} = 1$$

$$\alpha + \beta = 45°$$


*** Triangles 1/3 & 1/7

$$\tan\alpha = \unsur{3}$$

$$\tan\beta = \unsur{7}$$

$$\tan(\alpha+\beta) = \frac{1/3 + 1/7}{1 - \unsur{3 \cdot 7}}$$

$$\tan(\alpha+\beta) = \frac{10/21}{20/21} = \unsur{2}$$


*** Double du triangles 1/2 donne 4/3

$$\tan\alpha = \unsur{2}$$

$$\tan(2 \alpha) = \frac{2 \cdot 1/2}{1 - \unsur{2^2}} = \unsur{3/4} = 4/3$$


*** Double du triangle 1/3 donne 3/4

$$\tan\alpha = \unsur{3}$$

$$\tan(2 \alpha) =
\frac{2 \cdot 1/3}{1 - \unsur{3^2}} =
\frac{2/3}{8/9} =
\frac{2}{3} \frac{9}{8} = \frac{3}{4}$$


*** Double du triangle 1/$\phi$ donne 2/1

$$\tan\alpha = \unsur{\phi}$$

$$\tan(2 \alpha) = \frac{2/\phi}{1 - \unsur{\phi^2}}$$

$$\tan(2 \alpha) = \frac{2}{\phi} \cdot \frac{\phi^2}{\phi^2 - 1}$$

$$\tan(2 \alpha) = \frac{2}{\phi} \cdot \frac{\phi^2}{\phi} = 2$$


* Ratios


** Ratios du triangle équilatéral


*** Côté / Rayon cercle circonscrit




*** Cercles inscrit & circonscrit




** Ratios du carré


*** Cercles inscrit et circonscrit




** Ratios du pentagone




*** Côté pentagone / Rayon du cercle circonscrit




*** Diagonale pentagone / Côté
